# decomposition of Planck data into linear components
# python plank_data_linear_regression.py -v0 -w0


import numpy as np
from matplotlib import pyplot
import healpy
from optparse import OptionParser

def plaw(pars, x):
    """
        calculate a power-law function at x
        INPUT:
            pars = [norm, index]
            x - number or an array
        OUTPUT:
            f(x) = norm * x^index
    """
    return pars[0] * x**pars[1]


parser = OptionParser()

parser.add_option("-w", "--show", dest="show", default=0,
                  help="show the plots")
parser.add_option("-v", "--save", dest="save", default=0,
                  help="save the plots")

(options, args) = parser.parse_args()
show_plots = int(options.show)
save_plots = int(options.save)


test = 0

# these are all Planck frequencies
freqs = np.array([30, 44, 70, 100, 143, 217, 353, 545, 857])

# we will use the first 6 for the analysis
nfreq = 6
freqs = freqs[:nfreq]

# input Planck data
data = []
for freq in freqs:
    fn = 'Planck_data/Planck_%iGHz.npy' % freq
    data.append(np.load(fn))

# transform list into an array (npix by nfreq)
data = np.array(data).T
npix = data.shape[0]


# define a basis of model vectors, model_vectors = (nmodels by nfreqs) matrix
indices_dict = {'CMB': 0., 'Synchrotron': -2.7, 'Thermal dust': 2.2, 'Free-free': -2.1,}
comps = indices_dict.keys()
nmodels = len(comps)

model_vectors = np.zeros((nmodels, nfreq))

for ind, comp in enumerate(comps):
    pars = [1., indices_dict[comp]]
    model_vectors[ind] = plaw(pars, freqs)



# determine the Hessian = (nmodels by nmodels) matrix
hessian = np.dot(model_vectors, model_vectors.T)
Covar = np.linalg.inv(hessian)

# calculate the linear decomposition coefficients betas = (npix by nmodels) matrix
betas = np.dot(np.dot(data, model_vectors.T), Covar)

# plot the results at 70 GHz
nu_ind = 2
max = 300
for i, comp in enumerate(comps):
    if comp == 'CMB':
        min = -300
        continue
    elif comp == 'Synchrotron':
        min = -20
        max = 0
        
    else:
        min = 0
        continue
    title = '%s at %i GHz' % (comp, freqs[nu_ind])
    # multiply the maps by the spectrum at 70 GHz to get the emission at 70 GHz
    comp_map = betas.T[i] * model_vectors[i][nu_ind]
    
    # plot unsmoothed maps
    healpy.mollview(comp_map * 1.e6, title=title, min=min, max=max, unit=r'$\delta T_b\, (\rm \mu K)$')
    # smooth the maps with 0.5 deg Gaussian kernel and plot them
    sm_map = healpy.smoothing(comp_map, sigma=np.deg2rad(1.))
    healpy.mollview(sm_map * 1.e6, title=title + ' smoothed', min=min, max=max, unit=r'$\delta T_b\, (\rm \mu K)$')
    if save_plots:
        pyplot.savefig('plots/Planck_%s_component.pdf' % comp)

pyplot.show()