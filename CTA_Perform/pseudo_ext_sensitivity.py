import numpy as np
import matplotlib.pyplot as plt
#---own modules---#
import functions

#----------------------------------------------------------------------------
# this script calculates the (pseude) sensitivity to extended sources of CTA
# assuming the ROI = FOV/2
#----------------------------------------------------------------------------

#=== Input Info ===#
path2performance = '/home/vault/caph/sn0530/FB_for_CTA/CTA_data_challenge/Performance/'
obstime = ['0.5h', '5h', '50h']
erg2eV = 6.242e+11
#==================#


#=== Read file ===#
#Sensitivity
sensitivity=[]
for time in obstime:

	name4performance = 'CTA-Performance-South-20deg-DiffSens-%s.txt' %time

	file = open(path2performance+name4performance, 'r')
	lines = file.readlines()
	for lidx, l in enumerate(lines):
		if lidx>0:
			sensitivity.append(float(l.split('	')[2]))
	file.close()

sensitivity = np.asarray(sensitivity)
sensitivity = np.reshape(sensitivity, (3,20)) #sensitivity[0->0.5h|1->5h|2->50h][value] in [erg/cm2/s]
sensitivity = sensitivity*erg2eV*1e-9	#sensitivity in [GeV/cm2/s]


#energy
emin=[]
emax=[]
energy=[]
for time in obstime:

	name4performance = 'CTA-Performance-South-20deg-DiffSens-%s.txt' %time

	file = open(path2performance+name4performance, 'r')
	lines = file.readlines()
	for lidx, l in enumerate(lines):
		if lidx>0:
			emin.append(float(l.split('	')[0]))
			emax.append(float(l.split('	')[1]))
	file.close()

emin = np.asarray(emin)
emax = np.asarray(emax)
energy = (emin+emax)/2
energy = np.reshape(energy, (3,20)) #energy[0->0.5h|1->5h|2->50h][value] in [TeV]
#===================#


#=== Set up simulated ExpCutOff FB ===#
E0=2.		#[GeV]
E_cut=5e4	#[GeV]
k0=1e-6		#[ph/GeV/cm2/s/sr]
index=-2
FB = (energy[0]*1e3)**2 * functions.ExpCutOffPowerLaw(energy[0]*1e3, E0, E_cut, k0, index) #[GeV/cm2/s]
#===========================#


#=== Calc (pseudo) sensitivity to extended sources ===#
#area of a circle in [sr]
def area(radius):							#[sr]
	return np.pi**3 * ( radius/180 )**2		#radius [deg]

#intensity of (pseudo) extended source
def extend_intens(data, roi, psf):
	return data / ( np.sqrt(roi*psf) )

#radii
R_FOV=5.		#[deg]
R_PSF=0.05		#[deg] from the IceCube & HAWC estimations

#areas
PSF=area(R_PSF)					#[sr]
ROI=0.5 * area(R_FOV)			#[sr]

sensitivity = extend_intens(sensitivity, ROI, PSF) #[GeV/cm2/s/sr]
#=====================================================#


#=== Plotting ===#
#using LaTex for labels
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
#using defined colors
color, shape = functions.ColorAndMarker()

fig1 = plt.figure()
axis1=fig1.add_axes((.125,.125,0.75,0.75)) 	#left,bottom, width, height
axis1.set_xlabel(r'$\displaystyle E ~ \mathrm{[TeV]}$')
axis1.set_ylabel(r'$\displaystyle E^{2} \frac{dN}{dE} ~ \mathrm{[GeV cm^{-2} s^{-1} sr^{-1}]}$')
axis1.set_xlim(1e-2,3e2)
axis1.set_ylim(1e-8,5e-4)
axis1.grid(color=color[-2], linestyle='--', linewidth='0.25')

for tidx, time in enumerate(obstime):
	plt.loglog(energy[tidx],sensitivity[tidx], color[tidx], linewidth='2', label="CTA South %s" %time)

plt.loglog(energy[0],FB, color[-1], linestyle='--', label='sim. Fermi Bubbles')
axis1.legend(loc="best", framealpha=0.75)

fig1.savefig('pseudo_extended_source_sensitivity.pdf')
plt.show()
#================#










