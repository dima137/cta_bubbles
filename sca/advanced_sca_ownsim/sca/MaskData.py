import os
import numpy as np
import pyfits as pf
import matplotlib.pyplot as plt
#--- woen modules ---#
import functions

#-----------------------------------------------------------
#
#-----------------------------------------------------------

show_results=False
safe_results=False
finalplots=False

E_min = 0.03
E_max = 160
Ebins = 20
pxs = 0.02

irf = ["South_z20_average_50h", "prod3b-v1"]		#["irf", "caldb"]=["South_z20_average_50h", "prod3b-v1"] or ["South_z20_50h", "1dc"]
# irf = ["South_z20_50h", "1dc"]
sim_type = "gc_ExpCut_FermiBubbles_noBKG"	#[gc_noFB_noBKG | gc_ExpCut_FermiBubbles_noBKG | gc_ExpCut_FermiBubbles]
exrdef = "exr_2deg_stripe"	#[exr_1deg_stripe | exr_2deg_stripe]
modelpar = {
	# 'Softer':[1., 1., 1.e30, -3.],
	'Soft':[1., 1., 1.e30, -2.6],
	'Hard':[1., 1., 5.e7, -1.8],
	# 'Harder':[1., 1., 1.e30, -1.],
	}
new_pxs = 0.5
ebin_range = [5,9] #list, defines energy range for sca [start bin, end bin]=[int, int]
emin = 0.26	#TeV
emax = 2.19
t_obs = 180000

path2results = '%s/%i' %(exrdef, t_obs)




def MaskSky(sim_type=sim_type, bkg_type='bkg', exrdef=exrdef, modelpar=modelpar, irf=irf, E_min=E_min, E_max=E_max, Ebins=Ebins, pxs=pxs,
			ebin_range=ebin_range, emin=emin, emax=emax, new_pxs=new_pxs, ref_ypx=10.5, t_obs=t_obs,
			path2results=path2results, safe_results=safe_results, show_results=show_results, finalplots=finalplots):
	#INPUT
	#OUTPUT

	PATH = os.getenv('OWNSIM')

	# set filename in ../skymaps
	if 'Softer' not in modelpar.keys() and 'Harder' not in modelpar.keys():
		skyinfo = '%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
	if 'Softer' not in modelpar.keys() and 'Harder' in modelpar.keys():
		skyinfo = '%s_%s_Hard_%.2f_Harder_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Harder'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
	if 'Harder' not in modelpar.keys() and 'Softer' in modelpar.keys():
		skyinfo = '%s_%s_Hard_%.2f_Soft_%.2f_Softer_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], modelpar['Softer'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
	if 'Softer' in modelpar.keys() and 'Harder' in modelpar.keys():
		skyinfo = '%s_%s_Hard_%.2f_Harder_%.2f_Soft_%.2f_Softer_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Harder'][3], modelpar['Soft'][3], modelpar['Softer'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
	name4sky = 'hard_skymap_%s.fits' %skyinfo
	if functions.FileExist1('skymaps/'+name4sky)==False:
		print('>>> no skymap found <<<')
		print name4sky
		exit()

	name4softsky = 'soft_skymap_%s.fits' %skyinfo
	if functions.FileExist1('skymaps/'+name4sky)==False:
		print('>>> no skymap found <<<')
		print name4softsky
		exit()

	name4residual = 'resmap_%s.fits' %skyinfo
	if functions.FileExist1('skymaps/'+name4residual)==False:
		print('>>> no resmap found <<<')
		print name4residual
		exit()

	# # set filename in ../countscube
	# if 'Softer' not in modelpar.keys() and 'Harder' not in modelpar.keys():
	# 	new_cubeinfo = '%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
	# if 'Softer' not in modelpar.keys() and 'Harder' in modelpar.keys():
	# 	new_cubeinfo = '%s_%s_Hard_%.2f_Harder_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Harder'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
	# if 'Harder' not in modelpar.keys() and 'Softer' in modelpar.keys():
	# 	new_cubeinfo = '%s_%s_Hard_%.2f_Soft_%.2f_Softer_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], modelpar['Softer'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
	# if 'Softer' in modelpar.keys() and 'Harder' in modelpar.keys():
	# 	new_cubeinfo = '%s_%s_Hard_%.2f_Harder_%.2f_Soft_%.2f_Softer_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Harder'][3], modelpar['Soft'][3], modelpar['Softer'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)

	# # set filename in ../data
	# # cubeinfo = '%s_%s_%s_%.2fE%.2f_%iEbins_%.2fpxs_t%i' %(irf[1], irf[0], sim_type, E_min, E_max, Ebins, pxs, t_obs)
	# # countscube = PATH+'/countscube/cube_%s.fits' %cubeinfo
	# cube4fit = 'sca_data/cube4fit_%s.fits' %new_cubeinfo
	# if functions.FileExist1(cube4fit)==False:
	# 	print('>>> no cube4fit found <<<')
	# 	print cube4fit
	
	# set min/max for latitude band (which is masked)
	if exrdef=='exr_1deg_stripe':
		b_min = -.5
		b_max = .5
	if exrdef=='exr_2deg_stripe':
		b_min = -1.
		b_max = 1.

	# data of skymap
	# data = pf.open(cube4fit, mode='update')
	skymap = pf.open('skymaps/'+name4sky, mode='update')
	resmap = pf.open('skymaps/'+name4residual, mode='update')
	softsky = pf.open('skymaps/'+name4softsky, mode='update')
	# counts = data[0].data
	skydata = skymap[0].data
	resdata = resmap[0].data
	softdata = softsky[0].data

	# data of countscube
	# counts, cheader = functions.LoadFitsData(countscube, 0, printshape=False)
	# # if bkg is simulated in the data subtract simulated bkg from the data
	# if 'noBKG' in sim_type:
	# 	pass
	# else:
	# 	print('!!! subtract bkg !!!')
	# 	#load bkg data
	# 	bkginfo = '%s_%s_%s_%.2fE%.2f_%iEbins_%.2fpxs_t%i' %(irf[1], irf[0], bkg_type, E_min, E_max, Ebins, pxs, t_obs)
	# 	bkgcube = PATH+'/countscube/cube_%s.fits' %bkginfo
	# 	if functions.FileExist1(bkgcube)==False:
	# 		print('>>> no bkg cube found @ $OWNSIM/countscube <<<')
	# 		print bkgcube
	# 		exit()
	# 	bkg, bheader = functions.LoadFitsData(bkgcube, 0, printshape=False)
	# 	# subtract bkg from data(+bkg)
	# 	counts = counts - bkg
	# 	# set all counts<0 to zero
	# 	counts = counts*(1+np.sign(counts))/2
	# consider the desired energy bins
	# counts = counts[ebin_range[0]:ebin_range[1]+1,:,:]
	# # sum over these energy bins
	# counts = np.sum(counts, axis=0)
	# # resize pxs
	# npx = int(counts.shape[0])
	# n = int(new_pxs/pxs)
	# new_npx = int((npx/new_pxs)*pxs)
	# counts = np.reshape(counts, (new_npx, n, new_npx, n))
	# counts = np.sum(counts, axis=1)
	# counts = np.sum(counts, axis=2)
	
	# set all disered pixel in a degree stripe (b=0 +/- degree) to zero 
	for xidx in xrange(skydata.shape[1]):
		for yidx in xrange(skydata.shape[0]):
			
			b = functions.px2lat(yidx, ref_ypx, new_pxs)

			if b_min <= b < b_max:
				skydata[yidx][xidx]=0
				resdata[yidx][xidx]=0
				softdata[yidx][xidx]=0
			else:
				pass
	# set all disered pixel in the cube4fit in a degree stripe (b=0 +/- degree) to zero 
	# for e in xrange(counts.shape[0]):
	# 	for xidx in xrange(counts.shape[2]):
	# 		for yidx in xrange(counts.shape[1]):
				
	# 			b = functions.px2lat(yidx, ref_ypx, new_pxs)

	# 			if b_min <= b < b_max:
	# 				counts[e][yidx][xidx]=0
	# 			else:
	# 				pass

	# plot masked soft component map
	fig2 = plt.figure()
	plt.title('soft index %.1f' %modelpar['Soft'][3]+', E =  (%.2f - %.2f)TeV' %(emin, emax))
	plt.xlabel("longitude [$^\circ$]")
	plt.ylabel("latitude [$^\circ$]")
	ult = plt.imshow(softdata, origin="lower", extent=(5., -5., -5., 0.))#, interpolation='gaussian')
	cbar = plt.colorbar(ult, orientation='horizontal')
	cbar.set_label('ph. Counts')

	# plot masked hard component map
	fig1 = plt.figure()
	plt.title('hard index %.1f' %modelpar['Hard'][3]+', E =  (%.2f - %.2f)TeV' %(emin, emax))
	plt.xlabel("longitude [$^\circ$]")
	plt.ylabel("latitude [$^\circ$]")
	result = plt.imshow(skydata, origin="lower", extent=(5., -5., -5., 0.))#, interpolation='gaussian')
	cbar = plt.colorbar(result, orientation='horizontal')
	cbar.set_label('ph. Counts')

	# plot masked residual map
	fig0 = plt.figure()
	plt.title('residual, E =  (%.2f - %.2f)TeV' %(emin, emax))
	plt.xlabel("longitude [$^\circ$]")
	plt.ylabel("latitude [$^\circ$]")
	res = plt.imshow(resdata, origin="lower", extent=(5., -5., -5., 5.))#, interpolation='gaussian')
	cbar = plt.colorbar(res)
	cbar.set_label('ph. Counts')
	
	# safe/show results
	if safe_results==True:
		if finalplots==True:
			# fig0.savefig(path2results+'/residual_%s.eps' %new_cubeinfo)
			fig1.savefig(path2results+'/hard_%s.eps' %new_cubeinfo)
			fig2.savefig(path2results+'/soft_%s.eps' %new_cubeinfo)
		else:
			# fig0.savefig(path2results+'/residual_%s.png' %new_cubeinfo)
			fig1.savefig(path2results+'/hard_%s.png' %new_cubeinfo)
			fig2.savefig(path2results+'/soft_%s.png' %new_cubeinfo)
	
	if show_results==True:
		print('>>> PLOT RESULTS <<<')
		plt.show()
	if show_results==False:
		print('>>> CLOSE FIGURES <<<')
		plt.close('all')

	# print masked skymap to fits file
	# data.flush()
	# data.close()
	skymap.flush()
	skymap.close()
	softsky.flush()
	softsky.close()
	resmap.flush()
	resmap.close()
	
	# # print counts to fits file
	# data4sca = 'masked_sky_%s.fits' %new_cubeinfo
	# if functions.FileExist1('sca_data/'+data4sca)==False:
	# 	functions.Print2Fits(counts, new_pxs, ref_ypx, ref_ypx, 0., 0., 'sca_data/'+data4sca)
	# 	print('>>> print new cube within ebin range <<<')
	# else:
	# 	cube = pf.open('sca_data/'+data4sca, mode='update')
	# 	cube[0].data = counts
	# 	cube.flush()

	return 0




if __name__ == '__main__':

	MaskSky()



