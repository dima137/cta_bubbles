import os
import numpy as np
import pyfits as pf
import matplotlib.pyplot as plt
from matplotlib import colors as color
#--- own modules ---#
import functions
import OnOffMethod
import dio

#---------------------------------------------------------------------------
# this script applies the onoff method to the skymap of a sca component
# - directories needed /skymaps, /modelsky, /exrcubes, sca_error/, exrdef/t_obs,
#---------------------------------------------------------------------------

show_results=True
save_results=False
finalplots=False

#----IRF----
irf = ["South_z20_average_50h", "prod3b-v1"]		#["irf", "caldb"]=["South_z20_average_50h", "prod3b-v1"] or ["South_z20_50h", "1dc"]

#----CHOOSE WICH DATA TO USE------
sim_type = "gc_ExpCut_FermiBubbles_noBKG"	#[gc_noFB_noBKG | gc_ExpCut_FermiBubbles_noBKG | gc_ExpCut_FermiBubbles]
E_min = 0.03	#[TeV]
E_max = 160		#[TeV]
Ebins = 20		#[int]
pxs = 0.02		#[deg/px]
t_obs = 180000	#[s]

#----CHOOSE EXR DEFINITION----
exrdef = "exr_2deg_stripe"	#[exr_1deg_stripe | exr_2deg_stripe | exr_bright | exr_iem | exr_iem_hess]

#----SET PARAMETERS FOR ONOFF-METHOD----
OnOff = True #[True, False] if False the roi is plotted
#Parameter for onoff-method [degree]
l0=360.
l1=355.
b0=-2.
b1=-1.5
lbin_size=.5
bbin_size=.5

#----SET PARAMETERS FROM THE SCA----
#Used energy bin (used in sca.py)
ebin_range = [5,9]
emin_sca = 0.03	#energy edges of used_ebin [TeV]
emax_sca = 160.
#Data px size
new_pxs = 0.5
#Model | k0, E0, E_cut, index
modelpar = {'Soft':[1., 1., 1.e30, -2.6],
			'Hard':[1., 1., 5.e7, -1.9],
			}

path2results = '%s/%i' %(exrdef, t_obs)




def ApplyOnOffMethod(sim_type=sim_type, exrdef=exrdef, irf=irf, ebin_range=ebin_range, t_obs=t_obs, 
					modelpar=modelpar, emin_sca=emin_sca, emax_sca=emax_sca, sigma2=1.,
					E_min=E_min, E_max=E_max, Ebins=Ebins, pxs=pxs, new_pxs=new_pxs, new_xref=5.5, new_yref=5.5,
					l0=l0, l1=l1, b0=b0, b1=b1, lbin_size=lbin_size, bbin_size=bbin_size, OnOff=OnOff,
					path2results=path2results, show_results=show_results, save_results=save_results, finalplots=finalplots):
	# #INPUT
	# modelpar: dict of list {'modelname':[k0, E0, E_cut, index],...}
	# sim_type, exrdef: str
	# irf: list ['irf', 'caldb']
	# ebin_range: list [start,stop] in ebins
	# Ebins, t_obs: int
	# E_min, E_max: float [TeV]
	# l0, b0: float [degree]
	# pxs, new_pxs, lbin_size, bbin_size: float
	# show_results, safe_results, finalplots, OnOff: True, False
	
	# #OUTPUT
	# OnOff results: .png (or .pdf if finalplots=True)


	#---------------------------------------------------#
	#													#
	#		SET FILES USED BY THE ON-OFF ANALYSIS		#
	#													#
	#---------------------------------------------------#
	# set Path variable to the defined $OWNSIM environment variable
	PATH = os.getenv('OWNSIM')

	#onoff info
	onoff_info = '%.2fl%.2f_%.2fb%.2f' %(l0,l1,b0,b1)
	#model_info
	modinfo = 'fermi_bubbles_ExpCutOff_%s_%s_%.2fE%.2f_%iEbins_%.2fpxs_t%i' %(irf[1], irf[0], E_min, E_max, Ebins, pxs, t_obs)

	#Path to and name of the skymap
	path2sky = 'skymaps/'
	if 'Softer' not in modelpar.keys() and 'Harder' not in modelpar.keys():
		info = '%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin_sca, emax_sca, t_obs)
		name4sky = 'hard_skymap_%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.fits' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin_sca, emax_sca, t_obs)
	if 'Softer' not in modelpar.keys() and 'Harder' in modelpar.keys():
		info = '%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin_sca, emax_sca, t_obs)
		name4sky = 'hard_skymap_%s_%s_Hard_%.2f_Harder_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.fits' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Harder'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin_sca, emax_sca, t_obs)
	if 'Harder' not in modelpar.keys() and 'Softer' in modelpar.keys():
		info = '%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin_sca, emax_sca, t_obs)
		name4sky = 'hard_skymap_%s_%s_Hard_%.2f_Soft_%.2f_Softer_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.fits' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], modelpar['Softer'][3], irf[1], irf[0], new_pxs, emin_sca, emax_sca, t_obs)
	if 'Softer' in modelpar.keys() and 'Harder' in modelpar.keys():
		info = '%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin_sca, emax_sca, t_obs)
		name4sky = 'hard_skymap_%s_%s_Hard_%.2f_Harder_%.2f_Soft_%.2f_Softer_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.fits' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Harder'][3], modelpar['Soft'][3], modelpar['Softer'][3], irf[1], irf[0], new_pxs, emin_sca, emax_sca, t_obs)
	#Path to and name of model
	path2model = PATH+'/modelcube/'
	path2modelsky = 'modelsky/'
	name4model = 'modelcube_%s.fits' %modinfo
	name4modelsky = 'modelsky_fermi_bubbles_ExpCutOff_%s_%s_pxs%.2f_%.2fE%.2f_t%i.fits' %(irf[1], irf[0], new_pxs, emin_sca, emax_sca, t_obs)
	#Path to sca_error
	path2scaerror = 'sca_error/'
	if 'Softer' not in modelpar.keys() and 'Harder' not in modelpar.keys():
		name4scaerror = 'sigma2_%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.fits' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin_sca, emax_sca, t_obs)
	if 'Softer' not in modelpar.keys() and 'Harder' in modelpar.keys():
		name4scaerror = 'sigma2_%s_%s_Hard_%.2f_Harder_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.fits' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Harder'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin_sca, emax_sca, t_obs)
	if 'Harder' not in modelpar.keys() and 'Softer' in modelpar.keys():
		name4scaerror = 'sigma2_%s_%s_Hard_%.2f_Soft_%.2f_Softer_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.fits' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], modelpar['Softer'][3], irf[1], irf[0], new_pxs, emin_sca, emax_sca, t_obs)
	if 'Softer' in modelpar.keys() and 'Harder' in modelpar.keys():
		name4scaerror = 'sigma2_%s_%s_Hard_%.2f_Harder_%.2f_Soft_%.2f_Softer_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.fits' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Harder'][3], modelpar['Soft'][3], modelpar['Softer'][3], irf[1], irf[0], new_pxs, emin_sca, emax_sca, t_obs)

	# CHECK IF THERE IS SUCH MODEL IN $OWNSIM/modelcube/
	if functions.FileExist1(path2model+name4model)==False:
		print('>>> no modelcube found <<<')
		exit()
	else:
		pass


	#-------------------------------------------------------------------#
	#																	#
	#		LOAD DATA AND BRING IT IN RIGHT SHAPE FOR ONOF ANALYSIS		#
	#																	#
	#-------------------------------------------------------------------#
	print('>>> Setup Expected Model Counts <<<')
	#Load model cube and choose the right energy bin (used in sca.py) 
	model, mheader = functions.LoadFitsData(path2model+name4model, 0)
	# print('>>> FB model skymap <<<')
	model = functions.ResizePixel(model, new_pxs, printshape=False)
	model = model[ebin_range[0]:ebin_range[1]+1:1,:,:]
	model = np.sum(model, axis=0)
	# print model skymap of the considered ebin range (energy range) to fits
	fitsname = path2modelsky+name4modelsky
	# model references
	modref_x = model.shape[1]/2+.5
	modref_y = model.shape[0]/2+.5
	if functions.FileExist1(fitsname)==False:
		print('--- print new modelsky ---')
		functions.Print2Fits(model, new_pxs, modref_x, modref_y, 0., 0., fitsname)
	else:
		print('--- modlesky exists already - flush modelsky ---')
		modsky = pf.open(fitsname, mode='update')
		modsky[0].data = model
		modsky.flush()


	#---------------------------------------------------------------#
	#																#
	#		ON-OFF ANALYSIS ON (INTEGRATED) SCA DATA AND MODEL		#
	#																#
	#---------------------------------------------------------------#

	print('>>> OnOff Analysis <<<')
	#ROI and SIGNAL (sca data)
	skydata, sheader = functions.LoadFitsData(path2sky+name4sky, 0)
	if OnOff==True:
		signal = OnOffMethod.ONOFF(skydata, l0=l0, l1=l1, b0=b0, b1=b1, lbin_size=lbin_size, bbin_size=bbin_size, ref_ldeg=0., ref_bdeg=0., ref_xpx=new_xref, ref_ypx=new_yref, pxs=new_pxs)
	else:
		roi = OnOffMethod.ON(skydata, l0=l0, l1=l1, b0=b0, b1=b1, lbin_size=lbin_size, bbin_size=bbin_size, ref_ldeg=0., ref_bdeg=0., ref_xpx=new_xref, ref_ypx=new_yref, pxs=new_pxs)

	# print('\n>>> used modelsky: <<< \n%s'  %name4modelsky)
	#ROI and SIGNAL (FB model)
	modeldata, mheader = functions.LoadFitsData(path2modelsky+name4modelsky, 0)
	if OnOff==True:
		msignal = OnOffMethod.ONOFF(modeldata, l0=l0, l1=l1, b0=b0, b1=b1, lbin_size=lbin_size, bbin_size=bbin_size, ref_ldeg=0., ref_bdeg=0., ref_xpx=modref_x, ref_ypx=modref_y, pxs=new_pxs)
	else:
		mroi = OnOffMethod.ON(modeldata, l0=l0, l1=l1, b0=b0, b1=b1, lbin_size=lbin_size, bbin_size=bbin_size, ref_ldeg=0., ref_bdeg=0., ref_xpx=modref_x, ref_ypx=modref_y, pxs=new_pxs)


	#-----------------------------------------------------------#
	#															#
	#		CALCULATE THE SCA ERROR FOR THE ONOFF ANALYSIS		#
	#															#
	#-----------------------------------------------------------#
	#get sca error in On region of the OnOff-method
	uncertaintydata, uheader = functions.LoadFitsData(path2scaerror+name4scaerror, 0)
	unref_x = uncertaintydata.shape[1]/2+.5
	unref_y = uncertaintydata.shape[0]/2+.5
	sca_on_sigma_squared = OnOffMethod.ON(uncertaintydata, l0=l0, l1=l1, b0=b0, b1=b1, lbin_size=lbin_size, bbin_size=bbin_size, ref_ldeg=0., ref_bdeg=0., ref_xpx=unref_x, ref_ypx=unref_y, pxs=new_pxs)
	sca_off_sigma_squared = OnOffMethod.OFF(uncertaintydata, l0=l0, l1=l1, b0=b0, b1=b1, lbin_size=lbin_size, bbin_size=bbin_size, ref_ldeg=0., ref_bdeg=0., ref_xpx=unref_x, ref_ypx=unref_y, pxs=new_pxs)
	sca_err = np.sqrt(sca_on_sigma_squared+np.flip(sca_off_sigma_squared, 1))
	# #overall error
	error = sca_err# + onoff_err


	#-------------------------------#
	#								#
	#		PLOT ONOFF RESULTS		#
	#								#
	#-------------------------------#
	#LON AXIS
	region='ROI'
	n_lbins = int(np.abs(l1-l0)/lbin_size)
	lbins=np.arange(n_lbins, dtype=np.float)
	for s in xrange(len(lbins)):
		if region=='ROI':
			lbins[s] = (l0-.5)-s*lbin_size-360.
		elif region=='BKGR':
			lbins[s] = (l0+.5)+s*lbin_size

	# set ymax
	ymax = 2*np.amax(signal)

	# load list of colors and shapes
	colorlist, shape = functions.ColorAndMarker()

	#USE LATEX
	plt.rc('text', usetex=True)
	plt.rc('font', family='serif')

	#DEFINE FIGURE
	# fig1=plt.figure()
	fig1=plt.gcf()
	fig1.set_size_inches(9, 5.5)
	axis1=fig1.add_axes((.1,.1,0.8,0.8)) 	#left,bottom, width, height
	axis1.set_xlim(lbins[0]+.5, lbins[-1]-.5)
	axis1.grid(color=colorlist[-2], linestyle='--', linewidth='0.5', alpha=0.25)
	axis1.set_ylabel("ph. counts")
	axis1.set_xlabel("longitude [$^\circ$]")

	if OnOff==True:
		if np.amax(msignal)>np.amax(signal):
			plt.ylim(0, np.amax(msignal)+np.amax(error))
		if np.amax(signal)>np.amax(msignal):
			plt.ylim(0, np.amax(signal)+np.amax(error)) 
		for bidx in xrange(int(np.abs(b1-b0)/bbin_size)):
			axis1.set_title("On-Off method, E$_{sca}$ = (%.2f - %.2f)TeV, b=[%.2f, %.2f]$^\circ$" %(emin_sca,emax_sca,b0,b1))
			bmin = b0+bidx*bbin_size
			bmax = bmin + bbin_size
			axis1.errorbar(lbins, signal[bidx], yerr=error[bidx], capsize=3,
					color=colorlist[bidx], linestyle="", marker=shape[bidx], markersize='5', label="signal: [%.1f,%.1f]$^\circ$" %(bmin, bmax))
			# plt.plot(lbins, signal[0], color=colorlist[0], linestyle="-", marker=shape[0], label="on-off method")
			axis1.plot(lbins, msignal[bidx], colorlist[bidx], linestyle="--", label="model: [%.1f,%.1f]$^\circ$" %(bmin, bmax))
		if signal.shape[0]>4:
			# axis1.legend(bbox_to_anchor=(1.04,0.5), loc="center left", ncol=2, frameon=True, framealpha=0.25)
			axis1.legend(bbox_to_anchor=(.575,0.75), loc="center left", ncol=2, frameon=True, shadow=True, framealpha=1.)
		else:
			axis1.legend(loc='upper right', ncol=2, framealpha=0.25)

		if save_results==True:
			if finalplots==True:
				fig1.savefig(path2results+'/onoff_hard_%s_%s.pdf' %(onoff_info, info))
			else:
				fig1.savefig(path2results+'/onoff_hard_%s_%s.png' %(onoff_info, info))


	# #-------------------------------------------#
	# #											#
	# #		PLOT RESULTS FOR ON/OFF REGION 		#
	# #											#
	# #-------------------------------------------#
	# if OnOff==False:
	# 	axis1.set_title("ROI, E = (%.2f - %.2f)TeV, (b=[%.2f, %.2f]$^\circ$)" %(emin_sca,emax_sca,b0,b1))
	# 	plt.errorbar(lbins, roi[0], error[0], capsize=3, color=colorlist[0], linestyle="-", marker=shape[0], label="sca data")
	# 	plt.plot(lbins, mroi[0], colorlist[1], linestyle="-", marker=shape[1], label=" B model")
	# 	axis1.legend(loc="best", ncol=2)
	# 	if safe_results==True:
	# 		if finalplots==True:
	# 			fig1.savefig(path2results+'/roi_sca_%s_%s_hard_%.2f_%s_%.2fE%.2f_%s.pdf' %(irf[1],irf[0],modelpar['Hard'][3],sim_type,emin_sca,emax_sca,onoff_info))
	# 		else:
	# 			fig1.savefig(path2results+'/roi_sca_%s_%s_hard_%.2f_%s_%.2fE%.2f_%s.png' %(irf[1],irf[0],modelpar['Hard'][3],sim_type,emin_sca,emax_sca,onoff_info))


	#---------------------------#
	#							#
	#		SHOW RESULTS		#
	#							#
	#---------------------------#
	if show_results==True:
		print('>>> PLOT RESULTS <<<')
		plt.show()
		plt.close('all')
	if show_results==False:
		print('>>> CLOSE FIGURES <<<')
		plt.close('all')

	return 0




#=== MAIN ===#

if __name__ == '__main__':

	ApplyOnOffMethod()



