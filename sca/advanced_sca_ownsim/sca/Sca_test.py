import Sca


#----IRF----
irf = ["South_z20_50h", "1dc"]		#["irf", "caldb"]=["South_z20_average_50h", "prod3b-v1"] or ["South_z20_50h", "1dc"]
# irf = ["South_z20_average_50h", "prod3b-v1"]

#----CHOOSE WICH DATA TO USE------
sim_type = "gc_ExpCut_FermiBubbles" #[gc_noFB_noBKG | gc_ExpCut_FermiBubbles_noBKG | gc_ExpCut_FermiBubbles | sca_dummy_sources]
bkg_type = 'bkg'
E_min = 0.03	#[TeV]
E_max = 160		#[TeV]
Ebins = 100		#[int]
pxs = 0.02		#[deg/px]
new_pxs = 1.	#[deg/px]
t_obs = 180000	#[s]
#----CHOOSE EXR DEFINITION----
# exrdef = "exr_2deg_stripe"	#[exr_1deg_stipe | exr_2deg_stipe | exr_bright | exr_iem | exr_iem_hess]

#----SET MODELS----
# k0, E0, E_cut, index
modelpar = {'Soft':[1., 1., 1.e30, -2.7],
			'Hard':[1., 1., 5.e7, -1.8],
			}
#set used energy bin -> sets energy range
# used_ebin = 5	#1TeV-12TeV = bin 6 - 10
# ebin_range = [0,99]	#list, defines energy range for sca [start bin, end bin]=[int, int]
# ebin_range = [0,49]
ebin_range = [40,99]

Sca.ApplySCA(modelpar=modelpar, sim_type=sim_type, bkg_type=bkg_type, exrdef='sca_test', irf=irf, ebin_range=ebin_range, t_obs=t_obs,
			E_min=E_min, E_max=E_max, Ebins=Ebins, pxs=pxs, new_pxs=new_pxs,
			show_results=True, sca_test=False)
