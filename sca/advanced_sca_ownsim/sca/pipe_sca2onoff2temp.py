#------------------------------------------------------------------------
# this script pipes all subscripts used for the sca (in a certain energy range)
# and onoff method (in the same energy range)
#------------------------------------------------------------------------
#----IF TRUE RESULT ARE SAVED IN .EPS FORMAT (OTHERWISE .PNG)----#
finalplots = True
#----IF TRUE RESULT ARE PRINTED----
show_results = True
save_results = False
#----IF TRUE TEMPLATES ARE FITTED TO DATA----
ExecuteFit = True
# only one following parameter can be True
use_iTemp = False
use_sca_ibkg = False
#----CHOOSE WICH DATA TO USE------
#----SET THRESHOLDs (n sigma) FOR SCA FB TEMPLATE----
# the software looks for the best threshold to reconstruct the spectrum, but 0 < threshold < max_threshold
max_threshold = 5.
# setup cut for bright bkg source (outlier_sigma*error)
outlier_sigma = 2.
#---- LIST OF CONSIDERED TEMPLATES FOR FIT ----
temp_dct={'fb':True, 'hard':True, 'soft':True}	#{'fb':True, 'hard':True|False, 'soft':True|False}
												#{'fb':True, 'hard':True|False}
												#{'fb':True}
#----IRF----
# irf = ["South_z20_average_50h", "prod3b-v1"]		#["irf", "caldb"]=["South_z20_average_50h", "prod3b-v1"] or ["South_z20_50h", "1dc"]
irf = ["South_z20_50h", "1dc"]
sim_type = "gc_ExpCut_FermiBubbles_noBKG"	#[gc_noFB_noBKG | gc_ExpCut_FermiBubbles_noBKG | gc_ExpCut_FermiBubbles]
bkg_type = "bkg"	#[bkg, ] not implemented yet
#----ENERGY----
E_min = 0.03	#[TeV]
E_max = 160		#[TeV]
Ebins = 20		#[int]
#Used energy bin range [start,stop] (used in sca.py) 
ebin_range = [3,14] #ebin20 [3,7]=(0.11-0.93)TeV, ebin20 [8,13]=(0.93-12.19)TeV ||| ebin100 [14,67]=[0.10,10.27]TeV, ebin100 [40,]=[0.93,]
#Used energy range for fitting [start,stop] in TeV
fitted_erange = [.03,160.]
#----SIZE OF PX----
pxs = 0.02		#[deg/px]
new_pxs = .5	#[deg/px]
#----OBSERVATION TIME----
t_obs = [180000]#[1800,18000,90000,180000,900000]	#[s]
#----SET MODELS USED IN SCA----
# k0, E0, E_cut, index
modelpar = {
	'Soft':[1., 1., 1.e30, -2.7],
	'Hard':[1., 1., 5.e7, -2.]}
#----SET PARAMETERS FOR ONOFF-METHOD----
OnOff = True #[True, False] if False the roi is plotted
#Parameter for onoff-method [degree]
l0 = 360.
l1 = 357.
b0 = -2.
b1 = -1.
lbin_size = .5
bbin_size =	.5






























import os
import Sca
import ScaTemp
import FitData
import MaskData
import ApplyOnOff

#fixed parameters
exrdef = "exr_2deg_stripe"

for t in t_obs:

	print('\n#>>>>>>>>>>>>>>>>> ebin range [%i,%i], t_obs %i >>>>>>>>>>>>>>>>>#\n' %(ebin_range[0],ebin_range[1],t))

	print('\n\n#=::=::=:: APPLY SCA TO DATA ::=::=::=#\n\n')
	ellim_sca, eulim_sca, new_xref, new_yref, fit_xref, fit_yref, sigma2 = Sca.ApplySCA(
		modelpar=modelpar,
		sim_type=sim_type,
		bkg_type=bkg_type,
		exrdef=exrdef,
		irf=irf,
		ebin_range=ebin_range,
		t_obs = t,
		E_min=E_min,
		E_max=E_max,
		Ebins=Ebins,
		pxs=pxs,
		new_pxs=new_pxs,
		l0=l0,
		l1=l1,
		b0=b0,
		b1=b1,
		show_results=show_results,
		save_results=save_results,
		finalplots=finalplots)

	path2results = '%s/%i/%iEbins/%.2fE%.2f/%s' %(exrdef, t, Ebins, ellim_sca, eulim_sca, sim_type)
	if not os.path.exists(path2results):
		print('>>> make directory <<<')
		os.makedirs(path2results)

	# print('\n\n#=::=::=:: MASK DATA ::=::=::=#\n\n')
	# MaskData.MaskSky(
	# 	sim_type=sim_type,
	# 	bkg_type=bkg_type,
	# 	exrdef=exrdef,
	# 	modelpar=modelpar,
	# 	irf=irf,
	# 	emin=ellim_sca,
	# 	emax=eulim_sca,
	# 	Ebins=Ebins,
	# 	new_pxs=new_pxs,
	# 	ref_ypx=new_yref,
	# 	t_obs=t,
	# 	path2results=path2results,
	# 	safe_results=save_results,
	# 	show_results=show_results,
	# 	finalplots=finalplots)

	print('\n\n#=::=::=:: APPLY ONOFF ANALYSIS TO HARD SCA COMPONENT ::=::=::=#\n\n')
	ApplyOnOff.ApplyOnOffMethod(
		sigma2=sigma2,
		sim_type=sim_type,
		exrdef=exrdef,
		irf=irf,
		ebin_range=ebin_range,
		t_obs=t,
		modelpar=modelpar,
		emin_sca=ellim_sca,
		emax_sca=eulim_sca,
		E_min=E_min,
		E_max=E_max,
		Ebins=Ebins,
		pxs=pxs,
		new_pxs=new_pxs,
		new_xref=new_xref,
		new_yref=new_yref,
		l0=l0,
		l1=l1,
		b0=b0,
		b1=b1,
		lbin_size=lbin_size,
		bbin_size=bbin_size,
		OnOff=OnOff,
		path2results=path2results,
		show_results=show_results,
		save_results=save_results,
		finalplots=finalplots)

	print('\n\n#=::=::=:: SCA TEMPLATES ::=::=::=#\n\n')
	threshold = ScaTemp.ApplyScaTemp(
		threshold=max_threshold,
		outlier_sigma=outlier_sigma,
		sim_type=sim_type,
		exrdef=exrdef,
		modelpar=modelpar,
		irf=irf,
		E_min=E_min,
		E_max=E_max,
		Ebins=Ebins,
		pxs=pxs,
		new_pxs=new_pxs,
		new_xref=new_xref,
		new_yref=new_yref,
		fit_xref=fit_xref,
		fit_yref=fit_yref,
		ebin_range=ebin_range,
		emin=ellim_sca,
		emax=eulim_sca,
		t_obs=t,
		l0=l0,
		l1=l1,
		b0=b0,
		b1=b1,
		lbin_size=lbin_size,
		bbin_size=bbin_size,
		path2results=path2results,
		save_results=save_results,
		show_results=show_results,
		finalplots=finalplots)

	if ExecuteFit==True:
		print('\n\n#=::=::=:: FIT TEMPLATES TO DATA & CALC SPECTRUM ::=::=::=#\n\n')
		FitData.Fit2Data(
			threshold=threshold,
			temp_dct=temp_dct,
			fitted_erange=fitted_erange,
			sim_type=sim_type,
			exrdef=exrdef,
			modelpar=modelpar,
			irf=irf,
			pxs=pxs,
			Emin=E_min,
			Emax=E_max,
			nEbins=Ebins,
			t_obs=t,
			new_pxs=new_pxs,
			emin=ellim_sca,
			emax=eulim_sca,
			new_xref=new_xref,
			new_yref=new_yref,
			fit_xref=fit_xref,
			fit_yref=fit_yref,
			b0=b0,
			b1=b1,
			l0=l0,
			l1=l1,
			path2results=path2results,
			show_results=show_results,
			save_results=save_results,
			finalplots=finalplots,
			use_iTemp=use_iTemp,
			use_sca_ibkg=use_sca_ibkg)

	# print('\n\n#=::=::=:: RECONSTRUCT SCA SPECTRUM ::=::=::=#\n\n')
	# if 'noFB' in sim_type:
	# 	print('>>> no FB simulated <<<')
	# else:		
	# 	ScaTemp.RecoSpec(
	# 		threshold=threshold,
	# 		sim_type=sim_type,
	# 		exrdef=exrdef,
	# 		modelpar=modelpar,
	# 		irf=irf,
	# 		ellim=ellim_sca,
	# 		Emin=E_min,
	# 		eulim=eulim_sca,
	# 		Emax=E_max,
	# 		pxs=pxs,
	# 		new_pxs=new_pxs,
	# 		new_xref=new_xref,
	# 		new_yref=new_yref,
	# 		t_obs=t,
	# 		path2results=path2results,
	# 		save_results=save_results,
	# 		show_results=show_results,
	# 		finalplots=finalplots)




