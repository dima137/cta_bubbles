import os
import shutil
import numpy as np
import pyfits as pf
import matplotlib.pyplot as plt
#--- own modules ---#
import OnOffMethod
import functions
import dio

#---------------------------------------------------------------------------
# - calculating the hard, soft (harder, softer) component for a specific exrcube (exrcubes/<exrcube name>)
# - response cubes (exposure) has to be at $OWNSIM/response/ -> set path $OWNSIM as environment variable
# - results are saved at exrdef/t_obs/ -> e.g. exr_2deg_stripe/1800/
# - directories needed /skymaps, /modelsky, /exrcubes, sca_error/, exrdef/t_obs,
#---------------------------------------------------------------------------

finalplots = False
save_results = False

#----IRF----
# irf = ["South_z20_50h", "1dc"]		#["irf", "caldb"]=["South_z20_average_50h", "prod3b-v1"] or ["South_z20_50h", "1dc"]
irf = ["South_z20_average_50h", "prod3b-v1"]

#----CHOOSE WICH DATA TO USE------
sim_type = "gc_ExpCut_FermiBubbles_noBKG"	#[gc_noFB_noBKG | gc_ExpCut_FermiBubbles_noBKG | gc_ExpCut_FermiBubbles]
E_min = 0.03	#[TeV]
E_max = 160		#[TeV]
Ebins = 20		#[int]
pxs = 0.02		#[deg/px]
new_pxs = 0.5	#[deg/px]
t_obs = 180000	#[s]
#----CHOOSE EXR DEFINITION----
exrdef = "exr_2deg_stripe"	#[exr_1deg_stipe | exr_2deg_stipe | exr_bright | exr_iem | exr_iem_hess]

#----SET MODELS----
# k0, E0, E_cut, index
modelpar = {'Soft':[1., 1., 1.e30, -2.7],
			'Hard':[1., 1., 5.e7, -1.8],
			}
#set used energy bin -> sets energy range
# used_ebin = 5	#1TeV-12TeV = bin 6 - 10
ebin_range = [5,9] #list, defines energy range for sca [start bin, end bin]=[int, int]




#=== Implement methods ===#

#----------------------------------------------------------------------------------------------------------------------------------
# model of components depending on pixel and energy
#----------------------------------------------------------------------------------------------------------------------------------
# def SetupModel(ebin_range, energy, dE, exposure, px, modelpar, printshape=False):
def SetupModel(energy, exposure, px, modelpar, printshape=False):
	"""
	INPUT:
		ebin_range: list of two entries [start, stop] of used ebins (int)
		energy: center of energy bin (numpy array[nebins])
		dE: delta E of each energy bin (numpy array[nebins])
		exposure: exposure for each energy and pixel (numpy array[nebins][ypx][xpx])
		px: number of pixel on an axis of the data (eg. x)
		modelpar: dict of parameter for each component (dict = {'Comp1':listofpar, ...}, listofpar = [k0, E0, E_cut, index]) 
	OUTPUT:
		model: model at each energy and pixel (numpy array[nmodels][nebins][npx]) in [ph/MeV]
	"""

	# List of components
	components = modelpar.keys()

	# Number of models
	nmodels = len(components)
	# Number of energy bins
	nebins = exposure.shape[0]
	# Number of flatten px (ie. xpx*ypx)
	npx = px*px

	# Reshape exposure (nebins, npx)
	exposure = np.reshape(exposure, (nebins, npx))/np.amax(exposure)

	# Model array
	model = np.zeros((nmodels, nebins, npx))

	for cidx, comp in enumerate(components):
		# Set model parameters
		k0 = modelpar[comp][0]
		E0 = modelpar[comp][1]
		E_cut = modelpar[comp][2]
		index = modelpar[comp][3]
		# Calc model for each energy bin and pixel
		spectrum = functions.ExpCutPL(energy, E0, E_cut, k0, index) #(nebins)
		model[cidx] = (spectrum * exposure.T).T #(nmodels, nebins, npx)

	if printshape==True:
		print('>>> model shape: <<<')
		print(model.shape)

	return model, components




def MatricesOfProblem(data, sigma2, model, printshape=False):
	"""
	INPUT:
		data: data in units of ph.Counts/MeV and shape (ebins x npix)
		sigma: sigma^2=N_photons in each pixel and energy in the shape (ebins x npix)
		model: model from SetupModel()
	OUTPUT:
		F: matrix of the form (model' x model x npix)
		D: matrix of the form (model x npix)
	"""

	# Calc D matrix
	D = (data * model) / sigma2
	D = np.sum(D, axis=1)

	# Calc F matrix
	F = np.zeros((model.shape[0],model.shape[0],data.shape[0],data.shape[1]))
	for midx1 in xrange(model.shape[0]):
		F[midx1] = (model[midx1] * model) / sigma2
	F = np.sum(F, axis=2)

	if printshape==True:
		print('>>> D,F shape: <<<')
		print(D.shape, F.shape)

	return D, F




def Tmatrix(D, F, printshape=False):
	"""
	INPUT:
		D: matrix from MatricesOfProblem()
		F: matrix from MatricesOfProblem()
	OUTPUT:
		T: coefficient matrix of the form (nmodel x ypx x xpx)
	"""

	F_trans = F.T
	D_trans = D.T

	# #solve ax=b with python
	# T = np.zeros((F_trans.shape[0], F_trans.shape[1], F_trans.shape[2]))
	# for Eidx in xrange(F_trans.shape[1]):
	# 	for Pidx in xrange(F_trans.shape[0]):
	# 		if np.linalg.det(F_trans[Pidx][Eidx])==0:
	# 			continue
	# 		else:
	# 			T[Pidx][Eidx] = np.linalg.solve(F_trans[Pidx][Eidx], D_trans[Pidx][Eidx])

	# solve ax=b with python
	T = np.zeros((F_trans.shape[0], F_trans.shape[1]))
	for Pidx in xrange(F_trans.shape[0]):
			if np.linalg.det(F_trans[Pidx])==0:
				continue
			else:
				T[Pidx] = np.linalg.solve(F_trans[Pidx], D_trans[Pidx])

	T = T.T

	if printshape==True:
		print('>>> T shape: <<<')
		print(T.shape)

	return T





def ApplySCA(modelpar=modelpar, sim_type=sim_type, bkg_type='bkg', exrdef=exrdef, irf=irf, ebin_range=ebin_range, t_obs=t_obs,
			E_min=E_min, E_max=E_max, Ebins=Ebins, pxs=pxs, new_pxs=new_pxs,
			b0=-5., b1=-1., l0=360., l1=355.,
			show_results=False, save_results=False, finalplots=False, sca_test=False):
	# #INPUT
	# modelpar: dict of list {'modelname':[k0, E0, E_cut, index],...}
	# sim_type, exrdef: str
	# irf: list ['irf', 'caldb']
	# ebin_range: list [start, stop] in ebins 
	# Ebins, t_obs: int
	# E_min, E_max: float [TeV]
	# pxs, new_pxs: float
	# show_results, safe_results, finalplots: True, False

	# #OUTPUT
	# SCA results: .png (or .pdf if finalplots=True)


	#---------------------------------------#
	#										#
	#		SET FILES USED BY THE SCA		#
	#										#
	#---------------------------------------#
	# Set Path variable to the defined $OWNSIM
	PATH = os.getenv('OWNSIM')

	# Set info for observations and response
	cubeinfo = '%s_%s_%s_%.2fE%.2f_%iEbins_%.2fpxs_t%i' %(irf[1], irf[0], sim_type, E_min, E_max, Ebins, pxs, t_obs)
	modelinfo = '%s_%s_%.2fE%.2f_%iEbins_%.2fpxs_t%i' %(irf[1], irf[0], E_min, E_max, Ebins, pxs, t_obs)
	resinfo = '%s_%s_%.2fE%.2f_%iEbins_%.2fpxs_t%i' %(irf[1], irf[0], E_min, E_max, Ebins, pxs, t_obs)

	# Set variable for used countscube (exrcube) and response (expcube)
	countscube = PATH+"/countscube/cube_%s.fits" %cubeinfo
	exposurecube = PATH+"/response/expcube_%s.fits" %resinfo

	# CHECK IF THERE IS SUCH EXRCUBE IN ../exrcubes/
	if functions.FileExist1(countscube)==False:
		print('>>> no countscube found @ $OWNSIM/countscube <<<')
		print countscube
		exit()
	else:
		pass
	# CHECK IF THERE IS SUCH EXPCUBE IN $OWNSIM/response/
	if functions.FileExist1(exposurecube)==False:
		print('>>> no expcube found @ $OWNSIM/response <<<')
		print exposurecube
		exit()
	else:
		pass


	#---------------------------------------#
	#										#
	#		SET DATA USED BY THE SCA		#
	#										#
	#---------------------------------------#
	#=== Collect data for the sca ===#
	# print('>>> Load Data: counts, energy, dE, exposure <<<')
	counts, dheader = functions.LoadFitsData(countscube, 0)
	energy, dE = functions.CalcE(countscube)
	exposure = functions.LinInterOfExp(exposurecube)
	data = (counts.T/dE).T

	# # if bkg is simulated subtract simulated bkg data
	# if 'noBKG' in sim_type:
	# 	pass
	# else:
	# 	print('!>!>!> subtract bkg <!<!<!')
	# 	#load bkg data
	# 	bkginfo = '%s_%s_%s_%.2fE%.2f_%iEbins_%.2fpxs_t%i' %(irf[1], irf[0], bkg_type, E_min, E_max, Ebins, pxs, t_obs)
	# 	bkgcube = PATH+'/countscube/cube_%s.fits' %bkginfo
	# 	if functions.FileExist1(bkgcube)==False:
	# 		print('>>> no bkg cube found @ $OWNSIM/countscube <<<')
	# 		print bkgcube
	# 		exit()
	# 	bkg, bheader = functions.LoadFitsData(bkgcube, 0, printshape=False)
	# 	# subtract bkg from data(+bkg)
	# 	counts = counts - bkg
	# 	# set all counts<0 to zero (only for the sca)
	# 	counts = counts*(1+np.sign(counts))/2

	# set up arrays in the right index range corresponding to the ebin range used for the SCA
	dE = dE[ebin_range[0]:ebin_range[1]+1]
	energy = energy[ebin_range[0]:ebin_range[1]+1]
	exposure = exposure[ebin_range[0]:ebin_range[1]+1,:,:]
	counts = counts[ebin_range[0]:ebin_range[1]+1,:,:]

	# Energy [TeV] range of the ebin range used for SCA
	FOV=10.
	ellim_sca = (energy[0]-(dE[0]/2))*1e-6
	eulim_sca = (energy[-1]+(dE[-1]/2))*1e-6
	xyref = (FOV/new_pxs)/2+0.5

	#=== Rebinning ===#
	# print('>>> Resize pxs <<<')
	counts = functions.ResizePixel(counts, new_pxs)
	exposure = functions.ResizePixel(exposure, new_pxs)

	# 2D pxnumber
	px = counts.shape[1]

	print('>>> Emin/Emax = %.2f/%.2f for SCA <<<' %(ellim_sca, eulim_sca))


	#-------------------------------------------------------#
	#														#
	#		PLOT SCA DATA (INTEGRATED OVER ENERGIES)		#
	#														#
	#-------------------------------------------------------#
	#USE LATEX for plotting
	plt.rc('text', usetex=True)
	plt.rc('font', family='serif')

	# # Plot rebinned data
	# fig0 = plt.figure()
	# data = np.sum(counts, axis=0)
	# plt.imshow(data, origin="lower", extent=(5., -5., -5., 5.))#, interpolation='gaussian')
	# plt.title('data, E = (%.2f - %.2f)TeV' %(ellim_sca, eulim_sca))
	# plt.xlabel("longitude [$^\circ$]")
	# plt.ylabel("latitude [$^\circ$]")
	# cbar = plt.colorbar()
	# cbar.set_label('ph.Counts')
	# if save_results==True:
	# 	if finalplots==True:
	# 		save2path = exrdef+'/%i/data_%s_%s_%s_%.2fE%.2f' %(t_obs,irf[1],irf[0],sim_type,ellim_sca,eulim_sca)+'.pdf'
	# 	else:
	# 		save2path = exrdef+'/%i/data_%s_%s_%s_%.2fE%.2f' %(t_obs,irf[1],irf[0],sim_type,ellim_sca,eulim_sca)+'.png'


	#-------------------------------------------------------#
	#														#
	#		CALCULATE THE MATRICES OF THE PROPLEM D,F,T		#
	#														#
	#-------------------------------------------------------#
	#=== Set up model ===#
	print('>>> Setup Model <<<')
	# model, components = SetupModel(ebin_range, energy, dE, exposure, px, modelpar) # in [ph/MeV]
	model, components = SetupModel(energy, exposure, px, modelpar) # in [ph/MeV]

	#=== Reshape for sca ===#
	reshaped_counts = np.reshape(counts, (counts.shape[0], px*px))

	#=== sigma^2 = ph.Counts in each pixel (set all 0 to aurbitary value to avoid division by 0) ===#
	sigma2 = reshaped_counts
	for e in xrange(sigma2.shape[0]):
		for i in xrange(sigma2.shape[1]):
			if sigma2[e][i]==0:
				sigma2[e][i]=1.
			else:
				pass

	#=== Calc D,F matrix of the problem ===#
	print('>>> Calculate Matrices of the Problem <<<')
	D, F = MatricesOfProblem(reshaped_counts, sigma2, model)

	#=== Calc T matrix of the problem ===#
	T = Tmatrix(D, F)

	#=== Bring everything in the right form to plot ===#
	# T = T.reshape((model.shape[0], model.shape[1], px, px))
	T = T.reshape((model.shape[0], px, px))
	model = model.reshape((model.shape[0], model.shape[1], px, px))


	#-------------------------------------------------------------------------------------------#
	#																							#
	#		GENERATE BINNED SCA DATA (SCA ENERGY RANGE) WHICH CAN THAN BE USED BY CTLIKE		#
	#							-AND INTEGRATED DATA MAP-										#
	#-------------------------------------------------------------------------------------------#
	# set up cube4fit used by ctlike later
	# set info_temp
	if 'Softer' not in modelpar.keys() and 'Harder' not in modelpar.keys():
		info_temp = '%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, ellim_sca, eulim_sca, t_obs)
	if 'Softer' not in modelpar.keys() and 'Harder' in modelpar.keys():
		info_temp = '%s_%s_Hard_%.2f_Harder_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Harder'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, ellim_sca, eulim_sca, t_obs)
	if 'Harder' not in modelpar.keys() and 'Softer' in modelpar.keys():
		info_temp = '%s_%s_Hard_%.2f_Soft_%.2f_Softer_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], modelpar['Softer'][3], irf[1], irf[0], new_pxs, ellim_sca, eulim_sca, t_obs)
	if 'Softer' in modelpar.keys() and 'Harder' in modelpar.keys():
		info_temp = '%s_%s_Hard_%.2f_Harder_%.2f_Soft_%.2f_Softer_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Harder'][3], modelpar['Soft'][3], modelpar['Softer'][3], irf[1], irf[0], new_pxs, ellim_sca, eulim_sca, t_obs)

	print('>>> Setup SCA Data <<<')
	new_name4cube = 'cube4fit_%s.fits' %info_temp
	# remove old file
	if functions.FileExist1('sca_data/'+new_name4cube)==True:
		print('--- remove old cube4fit file ---')
		os.remove('sca_data/'+new_name4cube)
	# copy new file
	print('--- copy countscube ---')
	shutil.copy(countscube, 'sca_data/')
	# rename copied file
	print('--- rename copied file ---')
	name4cube = 'cube_%s.fits' %cubeinfo
	os.rename('sca_data/'+name4cube, 'sca_data/'+new_name4cube)

	# load data
	cube = pf.open('sca_data/'+new_name4cube, mode='update')
	cheader = cube[0].header
	counts2fit = cube[0].data
	weights = cube[1].data
	ebounds = cube[2].data

	# setup data cube to fit with ctlike later
	# resize px
	newpx2fit = functions.ResizePixel(counts2fit, new_pxs)
	# references for fit data
	new_fitxref = newpx2fit.shape[2]/2+.5
	new_fityref = newpx2fit.shape[1]/2+.5

	# set right px index to slice data
	b_low = functions.lat2px(b0, 0., new_fityref, new_pxs)
	b_high = functions.lat2px(b1, 0., new_fityref, new_pxs)
	l_left = functions.lon2px(360.-l1, 0., new_fitxref, new_pxs)
	l_right = functions.lon2px(l1, 0., new_fitxref, new_pxs)

	# flush cubedata to countscube
	# print('>>> FLUSH COUNTSCUBE <<<')
	print('--- write new data ---')
	# consider energy bins used in sca
	newpx2fit = newpx2fit[:,b_low:b_high,l_left:l_right]
	weights = weights[:,b_low:b_high,l_left:l_right]

	new_fitxref = newpx2fit.shape[2]/2+.5
	new_fityref = newpx2fit.shape[1]/2+.5

	# write to fits
	cube[0].data = newpx2fit
	cube[1].data = weights

	# write new header
	print('--- write new header ---')
	cheader['CRPIX1'] = new_fitxref
	cheader['CRPIX2'] = new_fityref
	cheader['CDELT1'] = -new_pxs
	cheader['CDELT2'] = new_pxs
	# write to file
	cube.flush()
	cube.close()


	# integrate over energies to detect pnt sources to model the bkg later
	int_newpx2fit = np.sum(newpx2fit, axis=0)
	# write map to fits file
	fitsname = 'map2fit_%s.fits' %info_temp
	if functions.FileExist1('sca_data/'+fitsname)==False:
		functions.Print2Fits(int_newpx2fit, new_pxs, new_fitxref, new_fityref, 0., 0., 'sca_data/'+fitsname)
	else:
		os.remove('sca_data/'+fitsname)
		functions.Print2Fits(int_newpx2fit, new_pxs, new_fitxref, new_fityref, 0., 0., 'sca_data/'+fitsname)


	print('>>> shapes: data, sigma2, model, D, F, T <<<')
	print reshaped_counts.shape, sigma2.shape, model.shape, D.shape, F.shape, T.shape

	#---------------------------------------------------------------------------------------------------#
	#																									#
	#		SUM OVER ALL ENERGIES OF SCA DATA TO APPLY THE ONOFF ANALYS LATE TO THE INTEGRATED DATA 	#
	#																									#
	#---------------------------------------------------------------------------------------------------#
	# sum over all energies to apply the OnOff Analysis later
	model = np.sum(model, axis=1)


	#-------------------------------------------------------#
	#														#
	#		SAVE THE F MATRIX (SIGMA^2) AS FITS FILE		#
	#														#
	#-------------------------------------------------------#
	print('>>> Setup SCA error <<<')
	# define file name for sigma^2 fits
	if 'Softer' not in modelpar.keys() and 'Harder' not in modelpar.keys():
		name4scaerror = 'sigma2_%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.fits' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, ellim_sca, eulim_sca, t_obs)
	if 'Softer' not in modelpar.keys() and 'Harder' in modelpar.keys():
		name4scaerror = 'sigma2_%s_%s_Hard_%.2f_Harder_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.fits' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Harder'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, ellim_sca, eulim_sca, t_obs)
	if 'Harder' not in modelpar.keys() and 'Softer' in modelpar.keys():
		name4scaerror = 'sigma2_%s_%s_Hard_%.2f_Soft_%.2f_Softer_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.fits' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], modelpar['Softer'][3], irf[1], irf[0], new_pxs, ellim_sca, eulim_sca, t_obs)
	if 'Softer' in modelpar.keys() and 'Harder' in modelpar.keys():
		name4scaerror = 'sigma2_%s_%s_Hard_%.2f_Harder_%.2f_Soft_%.2f_Softer_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.fits' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Harder'][3], modelpar['Soft'][3], modelpar['Softer'][3], irf[1], irf[0], new_pxs, ellim_sca, eulim_sca, t_obs)

	#calc sca error
	npx = int(10/new_pxs)
	F_trans = F.T
	F_inv = np.zeros_like(F_trans)
	# sigma2 = np.sum(sigma2, axis=0)
	sca_sigma_squared = np.zeros((sigma2.shape[1], sigma2.shape[0], F_trans.shape[1]))

	for pidx in xrange(F_trans.shape[0]):
		for eidx in xrange(sigma2.shape[0]):
			if np.linalg.det(F_trans[pidx])==0:
				sca_sigma_squared[pidx][eidx][:]=0
				continue
			else:
				F_inv[pidx] = np.linalg.inv(F_trans[pidx])
			sca_sigma_squared[pidx][eidx] = np.diag(F_inv[pidx])*np.diag(F_trans[pidx])*sigma2[eidx][pidx]

	sca_sigma_squared = np.sum(sca_sigma_squared, axis=1)

	#print sca errormap
	sca_sigma_squared = np.reshape(sca_sigma_squared, (npx, npx, sca_sigma_squared.shape[1])).T
	sca_sigma_squared = sca_sigma_squared[0].T
	if functions.FileExist2('sca_error/', name4scaerror)==False:
		print('--- print new sigma2 sky ---')
		functions.Print2Fits(sca_sigma_squared, new_pxs, xyref, xyref, 0., 0., 'sca_error/'+name4scaerror)
	else:
		print('--- sigma2 sky exists already - flush sigma2 ---')
		errorsky = pf.open('sca_error/'+name4scaerror, mode='update')
		errorsky[0].data = sca_sigma_squared
		errorsky.flush()


	#-------------------------------------------------------------------#
	#																	#
	#		GENERATE THE (INTEGRATED) HARD AND SOFT COMPONENT MAPS  	#
	#																	#
	#-------------------------------------------------------------------#
	print('>>> Setup SCA Components <<<')
	# name for skymap of the integrated sca data used by the onoff analysis
	if 'Softer' not in modelpar.keys() and 'Harder' not in modelpar.keys():
		name4sky = 'hard_skymap_%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.fits' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, ellim_sca, eulim_sca, t_obs)
	if 'Softer' not in modelpar.keys() and 'Harder' in modelpar.keys():
		name4sky = 'hard_skymap_%s_%s_Hard_%.2f_Harder_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.fits' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Harder'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, ellim_sca, eulim_sca, t_obs)
	if 'Harder' not in modelpar.keys() and 'Softer' in modelpar.keys():
		name4sky = 'hard_skymap_%s_%s_Hard_%.2f_Soft_%.2f_Softer_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.fits' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], modelpar['Softer'][3], irf[1], irf[0], new_pxs, ellim_sca, eulim_sca, t_obs)
	if 'Softer' in modelpar.keys() and 'Harder' in modelpar.keys():
		name4sky = 'hard_skymap_%s_%s_Hard_%.2f_Harder_%.2f_Soft_%.2f_Softer_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.fits' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Harder'][3], modelpar['Soft'][3], modelpar['Softer'][3], irf[1], irf[0], new_pxs, ellim_sca, eulim_sca, t_obs)
	# name for soft skymap of the integrated sca data used for bkg model
	if 'Softer' not in modelpar.keys() and 'Harder' not in modelpar.keys():
		name4softsky = 'soft_skymap_%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.fits' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, ellim_sca, eulim_sca, t_obs)
	if 'Softer' not in modelpar.keys() and 'Harder' in modelpar.keys():
		name4softsky = 'soft_skymap_%s_%s_Hard_%.2f_Harder_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.fits' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Harder'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, ellim_sca, eulim_sca, t_obs)
	if 'Harder' not in modelpar.keys() and 'Softer' in modelpar.keys():
		name4softsky = 'soft_skymap_%s_%s_Hard_%.2f_Soft_%.2f_Softer_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.fits' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], modelpar['Softer'][3], irf[1], irf[0], new_pxs, ellim_sca, eulim_sca, t_obs)
	if 'Softer' in modelpar.keys() and 'Harder' in modelpar.keys():
		name4softsky = 'soft_skymap_%s_%s_Hard_%.2f_Harder_%.2f_Soft_%.2f_Softer_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.fits' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Harder'][3], modelpar['Soft'][3], modelpar['Softer'][3], irf[1], irf[0], new_pxs, ellim_sca, eulim_sca, t_obs)

	# new references for map due to the new_pxs
	# resize px
	cube4newpx = functions.ResizePixel(counts2fit, new_pxs)
	# references for fit data
	new_xref = cube4newpx.shape[2]/2+.5
	new_yref = cube4newpx.shape[1]/2+.5
	# set right px index to slice data
	b_low = functions.lat2px(-5, 0., new_yref, new_pxs)
	b_high = functions.lat2px(b1, 0., new_yref, new_pxs)


	listofcomp=[]
	#=== Plot results ===#
	for j, mod in enumerate(components):

		# set up map for each component of the model
		comp_map = T[j] * model[j]

		#===== print soft component to fits file  =======
		if mod=='Soft' and functions.FileExist1('skymaps/'+name4softsky)==False:
			#Sety all negative counts to zero -> negative counts in hard comonent are in soft component?
			soft_comp = np.zeros_like(comp_map)
			for y in xrange(comp_map.shape[0]):
				for x in xrange(comp_map.shape[1]):
					# if comp_map[y][x]>0:
					# 	soft_comp[y][x]=comp_map[y][x]
					soft_comp[y][x]=comp_map[y][x]
			# consider only negative latitudes
			soft_comp = soft_comp[b_low:b_high,:]
			#print new fits
			print('--- print new soft skymap ---')
			functions.Print2Fits(soft_comp, new_pxs, new_xref, new_yref, 0., 0., 'skymaps/'+name4softsky)
		elif mod=='Soft' and functions.FileExist1('skymaps/'+name4softsky)==True:
			#Sety all negative counts to zero -> negative counts in hard comonent are in soft component?
			soft_comp = np.zeros_like(comp_map)
			for y in xrange(comp_map.shape[0]):
				for x in xrange(comp_map.shape[1]):
					# if comp_map[y][x]>0:
					# 	soft_comp[y][x]=comp_map[y][x]
					soft_comp[y][x]=comp_map[y][x]
			# consider only negative latitudes
			soft_comp = soft_comp[b_low:b_high,:]
			# flush existing fits
			print('--- soft skymap exists already - flush skymap ---')
			sky = pf.open('skymaps/'+name4softsky, mode='update')
			sky[0].data = soft_comp
			sky.flush()
			sky.close()

		# print component map to a skymap needed for the onoff method after the sca
		skymap4countscube = 'skymaps/'+name4sky
		if mod=='Hard' and functions.FileExist1(skymap4countscube)==False:
			
			#Sety all negative counts to zero -> negative counts in hard comonent are in soft component?
			hard_comp = np.zeros_like(comp_map)
			for y in xrange(comp_map.shape[0]):
				for x in xrange(comp_map.shape[1]):
					# if comp_map[y][x]>0:
						# hard_comp[y][x]=comp_map[y][x]
					hard_comp[y][x]=comp_map[y][x]
			# consider only negative latitudes
			hard_comp = hard_comp[b_low:b_high,:]
			#print new fits
			print('--- print new hard skymap ---')
			functions.Print2Fits(hard_comp, new_pxs, new_xref, new_yref, 0., 0., skymap4countscube)

		elif mod=='Hard' and functions.FileExist1(skymap4countscube)==True:

			#Sety all negative counts to zero -> negative counts in hard comonent are in soft component?
			hard_comp = np.zeros_like(comp_map)
			for y in xrange(comp_map.shape[0]):
				for x in xrange(comp_map.shape[1]):
					# if comp_map[y][x]>0:
						# hard_comp[y][x]=comp_map[y][x]
					hard_comp[y][x]=comp_map[y][x]
			# consider only negative latitudes
			hard_comp = hard_comp[b_low:b_high,:]
			# flush existing fits
			print('--- hard skymap exists already - flush skymap ---')
			sky = pf.open(skymap4countscube, mode='update')
			sky[0].data = hard_comp
			sky.flush()
			sky.close()

		# append each component map to a list -> later the sum of all component maps are needed to calc residual map
		listofcomp.append(comp_map)

		#---------------------------------------------------------------#
		#																#
		#		PLOT THE (INTEGRATED) HARD AND SOFT COMPONENT MAPS  	#
		#																#
		#---------------------------------------------------------------#
		# plot component map for each component of the model in the considere energy bin range (energy range)
		fig_comp = plt.figure()
		ax_comp = fig_comp.add_axes((0.1,0.1,0.8,0.8))
		ax_comp.set_title(mod+' index %.1f' %modelpar[mod][3]+', E =  (%.2f - %.2f)TeV' %(ellim_sca, eulim_sca))
		ax_comp.set_xlabel("longitude [$^\circ$]")
		ax_comp.set_ylabel("latitude [$^\circ$]")
		if mod=='Hard':
			result = ax_comp.imshow(hard_comp, origin="lower", extent=(5., -5., -5., b1))#, interpolation='gaussian')
		if mod=='Soft':
			result = ax_comp.imshow(soft_comp, origin="lower", extent=(5., -5., -5., b1))#, interpolation='gaussian')
		cbar = plt.colorbar(result, orientation='horizontal')
		cbar.set_label('ph. counts')
		
		if save_results==True:
			# create path2results
			path2results = '%s/%i/%iEbins/%.2fE%.2f/%s' %(exrdef, t_obs, Ebins, ellim_sca, eulim_sca, sim_type)
			if not os.path.exists(path2results):
				print('>>> make directory <<<')
				os.makedirs(path2results)
			# save fig
			fig_comp.savefig(path2results+'/sca_%s_%s.pdf' %(mod, info_temp))


	#---------------------------------------#
	#										#
	#		RESIDUALS OF THE COMP MAPS  	#
	#										#
	#---------------------------------------#
	print('>>> Setup Residual for the SCA <<<')
	#=== Sum up all components and subtract it from the Data ===#
	sumdat = np.zeros_like(comp_map)
	for l in xrange(len(listofcomp)):
		sumdat += listofcomp[l]

	# calc residual counts of all consider energy bins - sum of components
	counts = np.sum(counts, axis=0)
	residual = counts - sumdat
	# consider negative latitudes
	residual = residual[b_low:b_high,:]

	# plot residual map
	fig_residual = plt.figure()
	ax_residual = fig_residual.add_axes((0.1,0.1,0.8,0.8))
	ax_residual.set_title('Residuals, E = (%.2f - %.2f)TeV' %(ellim_sca, eulim_sca))
	ax_residual.set_xlabel("longitude [$^\circ$]")
	ax_residual.set_ylabel("latitude [$^\circ$]")
	res = ax_residual.imshow(residual, origin="lower", extent=(5., -5., -5., b1))#, interpolation='gaussian')
	cbar_residual = plt.colorbar(res, orientation='horizontal')
	cbar_residual.set_label('ph. counts')

	if save_results==True:
		if finalplots==True:
			fig_residual.savefig(path2results+'/sca_Res_%s.pdf' %info_temp)
		else:
			fig_residual.savefig(path2results+'/sca_Res_%s.png' %info_temp)
	
	#---------------------------#
	#							#
	#		TEST CHI^2 FIT   	#
	#							#
	#---------------------------#
	test_chi2 = False
	if test_chi2==True:
		plt.close('all')
		print('\n>>> Test Chi^2 Fit <<<\n')

		ebins=10
		ny = soft_comp.shape[0]
		nx = soft_comp.shape[1]
		SDATA=np.zeros((ebins, ny, nx))
		HDATA=np.zeros((ebins, ny, nx))
		for e in xrange(ebins):
			SDATA[e] = np.random.poisson(soft_comp)
			HDATA[e] = np.random.poisson(hard_comp)
		DATA = SDATA+HDATA
		SIGMA2 = np.zeros((ebins))
		for e in xrange(ebins):
			SIGMA2[e] = np.sum(DATA[e])/nx/ny
		TEMP = np.zeros((2, ny, nx))
		TEMP[0] = soft_comp
		TEMP[1] = hard_comp 

		DATA = DATA.reshape(ebins, nx*ny)
		TEMP = TEMP.reshape(2, nx*ny)

		import FitData
		D, M = FitData.CalcMatrices(DATA, TEMP, SIGMA2)
		F = FitData.RunFit(D, M)

		RESIDUALS = DATA - np.dot(F, TEMP)
		RESIDUALS = RESIDUALS.reshape((ebins, ny, nx))
		RESIDUALS = np.sum(RESIDUALS, axis=0)
		OVERALL = np.sum(RESIDUALS)
		print OVERALL

		fig_r = plt.figure()
		plt.title('Residuals, (over all residual = %.2f)' %OVERALL)
		r = plt.imshow(RESIDUALS, origin='lower', extent=[5,-5,-5,-1])
		plt.colorbar(r, orientation='horizontal')
		fig_r.savefig('Test_Chi2_Residuals.png')
		plt.show()




	# test chi^2 fit with random poisson values

	#-----------------------------------#
	#									#
	#		SAVE AND SHOW RESULTS   	#
	#									#
	#-----------------------------------#
	# if finalplots==True:
	# 	save2path = exrdef+'/%i/residual_%s_%s_Hard_%.2f_%s_%.2fE%.2f' %(t_obs,irf[1],irf[0],modelpar['Hard'][3],sim_type,ellim_sca,eulim_sca)+'.pdf'
	# else:
	# 	save2path = exrdef+'/%i/residual_%s_%s_Hard_%.2f_%s_%.2fE%.2f' %(t_obs,irf[1],irf[0],modelpar['Hard'][3],sim_type,ellim_sca,eulim_sca)+'.png'

	# if sca_test==True:
	# 	fig0.savefig(exrdef+'/data_%.2fE%.2f.pdf' %(ellim_sca, eulim_sca))
	# 	fig2.savefig(exrdef+'/residual_%.2fE%.2f.pdf' %(ellim_sca, eulim_sca))

	if show_results==True:
		print('>>> PLOT RESULTS <<<')
		plt.show()
		plt.close('all')
	if show_results==False:
		print('>>> CLOSE FIGURES <<<')
		plt.close('all')


	return ellim_sca, eulim_sca, new_xref, new_yref, new_fitxref, new_fityref, sigma2


#=== MAIN ===#

if __name__ == '__main__':

	ellim_sca, eulim_sca, xyref, sigma2, sca_err = ApplySCA()

