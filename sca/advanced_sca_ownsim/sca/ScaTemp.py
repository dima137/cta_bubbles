import os
import shutil
import ctools
import cscripts
import gammalib
import numpy as np
import pyfits as pf
import matplotlib.pyplot as plt
import xml.etree.ElementTree as et
#--- own modules ---#
import dio
import functions
import OnOffMethod
import InOutTemp

#--------------------------------------------------------------------------
# this script recovers the FB template and compares it to the input
#--------------------------------------------------------------------------

show_results=True
save_results=False
finalplots=False

#---Data info---
exrdef = "exr_2deg_stripe"	#[exr_1deg_stripe | exr_2deg_stripe]
# irf = ["South_z20_average_50h", "prod3b-v1"]		#["irf", "caldb"]=["South_z20_average_50h", "prod3b-v1"] or ["South_z20_50h", "1dc"]
irf =["South_z20_50h", "1dc"]
sim_type = "gc_ExpCut_FermiBubbles_noBKG"	#[gc_noFB_noBKG | gc_ExpCut_FermiBubbles_noBKG | gc_ExpCut_FermiBubbles]
t_obs = 180000	#[s]
Ebins = 20
ebin_range = [0,Ebins-1]
E_min = 0.03	#[TeV]
E_max = 160.	#[TeV]
emin = 0.03		#[TeV]
emax = 160.		#[TeV]
pxs = 0.02		#[deg/px]
new_pxs = 1.	#[deg/px]

#---Define the ROI for the sca FB template---
#[float]
l0=360.
l1=356.
b0=-5.
b1=-1.
lbin_size=1.
bbin_size=1.

#---Model---
# k0, E0, E_cut, index
modelpar = {'Soft':[1., 1., 1.e30, -2.7], 
			'Hard':[1., 1., 5.e7, -1.9]
			}

path2results = '%s/%i' %(exrdef, t_obs)






#==================================================================================================================================
#==================================================================================================================================
#==================================================================================================================================





def ApplyScaTemp(threshold=2, outlier_sigma=2.,
				sim_type=sim_type, exrdef=exrdef, modelpar=modelpar, irf=irf, E_min=E_min, E_max=E_max, Ebins=Ebins,
				pxs=pxs, new_pxs=new_pxs, new_xref=5.5, new_yref=5.5, fit_xref=5.5, fit_yref=5.5, ebin_range=ebin_range, emin=emin, emax=emax, t_obs=t_obs,
				l0=l0, l1=l1, b0=b0, b1=b1, lbin_size=lbin_size, bbin_size=bbin_size,
				path2results=path2results, save_results=False, show_results=False, finalplots=False, use_iTemp=False):
	#INPUT
	#OUTPUT


	#-----------------------------------------------#
	#												#
	#		SET FILE NAMES USED BY ApplyScaTemp		#
	#												#
	#-----------------------------------------------#
	# PATH TO SIMULATIONS
	PATH = os.getenv('OWNSIM')

	# FILE NAMES
	# name of the simulated countscube for ctlike
	name4cube = 'cube_%s_%s_%s_%.2fE%.2f_%iEbins_%.2fpxs_t%i.fits' %(irf[1], irf[0], sim_type, E_min, E_max, Ebins, pxs, t_obs)
	# name for modelsky needed for trust level (is the template pixel really due to the Fermi Bubbles)
	name4model = 'modelsky_fermi_bubbles_ExpCutOff_%s_%s_pxs%.2f_%.2fE%.2f_t%i.fits' %(irf[1], irf[0], new_pxs, emin, emax, t_obs)
	if functions.FileExist1('modelsky/'+name4model)==False:
		print('>>> no modelsky found <<<')
		print name4model
		exit()

	# neme for the hard sca component skymap file
	if 'Softer' not in modelpar.keys() and 'Harder' not in modelpar.keys():
		info = '%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
		name4sky = 'hard_skymap_%s.fits' %info
		name4softsky = 'soft_skymap_%s.fits' %info
		name4fitcube = 'cube4fit_%s.fits' %info
	if 'Softer' not in modelpar.keys() and 'Harder' in modelpar.keys():
		info = '%s_%s_Hard_%.2f_Harder_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Harder'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
		name4sky = 'hard_skymap_%s.fits' %info
		name4softsky = 'soft_skymap_%s.fits' %info
		name4fitcube = 'cube4fit_%s.fits' %info
	if 'Harder' not in modelpar.keys() and 'Softer' in modelpar.keys():
		info = '%s_%s_Hard_%.2f_Soft_%.2f_Softer_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], modelpar['Softer'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
		name4sky = 'hard_skymap_%s.fits' %info
		name4softsky = 'soft_skymap_%s.fits' %info
		name4fitcube = 'cube4fit_%s.fits' %info
	if 'Softer' in modelpar.keys() and 'Harder' in modelpar.keys():
		info = '%s_%s_Hard_%.2f_Harder_%.2f_Soft_%.2f_Softer_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Harder'][3], modelpar['Soft'][3], modelpar['Softer'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
		name4sky = 'hard_skymap_%s.fits' %info
		name4softsky = 'soft_skymap_%s.fits' %info
		name4fitcube = 'cube4fit_%s.fits' %info
	if functions.FileExist1('skymaps/'+name4sky)==False:
		print('>>> no skymap found <<<')
		print name4sky
		exit()
	if functions.FileExist1('skymaps/'+name4softsky)==False:
		print('>>> no soft skymap found <<<')
		print name4softsky
		exit()
	if functions.FileExist1('sca_data/'+name4fitcube)==False:
		print('>>> no cube4fit found <<<')
		print name4fitcube
		exit()

	# name for the sca sigma^2 file
	if 'Softer' not in modelpar.keys() and 'Harder' not in modelpar.keys():
		name4scaerror = 'sigma2_%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.fits' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
	if 'Softer' not in modelpar.keys() and 'Harder' in modelpar.keys():
		name4scaerror = 'sigma2_%s_%s_Hard_%.2f_Harder_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.fits' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Harder'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
	if 'Harder' not in modelpar.keys() and 'Softer' in modelpar.keys():
		name4scaerror = 'sigma2_%s_%s_Hard_%.2f_Soft_%.2f_Softer_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.fits' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], modelpar['Softer'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
	if 'Softer' in modelpar.keys() and 'Harder' in modelpar.keys():
		name4scaerror = 'sigma2_%s_%s_Hard_%.2f_Harder_%.2f_Soft_%.2f_Softer_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.fits' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Harder'][3], modelpar['Soft'][3], modelpar['Softer'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
	if functions.FileExist1('sca_error/'+name4scaerror)==False:
		print('>>> no sigma^2 found <<<')
		print name4scaerror
		exit()

	# load data from fits files
	fitdata, fitheader = functions.LoadFitsData('sca_data/'+name4fitcube, 0)
	scadata, dheader = functions.LoadFitsData('skymaps/'+name4sky, 0)
	bkgdata, bkgheader = functions.LoadFitsData('skymaps/'+name4softsky, 0)
	modeldata, mheader = functions.LoadFitsData('modelsky/'+name4model, 0)
	sigma2, sheader = functions.LoadFitsData('sca_error/'+name4scaerror, 0)

	# references for model and sigma2 data (for sca data input parameter)
	modref_x = modeldata.shape[1]/2+.5
	modref_y = modeldata.shape[0]/2+.5
	sigref_x = sigma2.shape[1]/2+.5
	sigref_y = sigma2.shape[0]/2+.5


	#---------------------------------------#
	#										#
	#		DATA/ERROR/SIGNIFICANCE 		#
	#										#
	#---------------------------------------#
	# on-off method to calc the FB signal
	print('>>> Define Template map <<<')
	signal = OnOffMethod.ONOFF(scadata, l0=l0, l1=l1, b0=b0, b1=b1, lbin_size=lbin_size, bbin_size=bbin_size, ref_ldeg=0., ref_bdeg=0., ref_xpx=new_xref, ref_ypx=new_yref, pxs=new_pxs)
	mod = OnOffMethod.ONOFF(modeldata, l0=l0, l1=l1, b0=b0, b1=b1, lbin_size=lbin_size, bbin_size=bbin_size, ref_ldeg=0., ref_bdeg=0., ref_xpx=modref_x, ref_ypx=modref_y, pxs=new_pxs)
	hard_on = OnOffMethod.ON(scadata, l0=l0, l1=l1, b0=b0, b1=b1, lbin_size=lbin_size, bbin_size=bbin_size, ref_ldeg=0., ref_bdeg=0., ref_xpx=new_xref, ref_ypx=new_yref, pxs=new_pxs)
	soft_off = OnOffMethod.OFF(bkgdata, l0=l0, l1=l1, b0=b0, b1=b1, lbin_size=lbin_size, bbin_size=bbin_size, ref_ldeg=0., ref_bdeg=0., ref_xpx=new_xref, ref_ypx=new_yref, pxs=new_pxs)

	# uncertainty
	print('>>> Calc. Significance <<<')
	sigma2_on = OnOffMethod.ON(sigma2, l0=l0, l1=l1, b0=b0, b1=b1, lbin_size=lbin_size, bbin_size=bbin_size, ref_ldeg=0., ref_bdeg=0., ref_xpx=sigref_x, ref_ypx=sigref_y, pxs=new_pxs)
	sigma2_off = OnOffMethod.OFF(sigma2, l0=l0, l1=l1, b0=b0, b1=b1, lbin_size=lbin_size, bbin_size=bbin_size, ref_ldeg=0., ref_bdeg=0., ref_xpx=sigref_x, ref_ypx=sigref_y, pxs=new_pxs)
	err = np.sqrt(sigma2_on+np.flip(sigma2_off, 1))

	# solid angle of a single px [sr]
	dO = functions.dOmega(new_pxs) 

	# significance
	sign = np.zeros_like(signal)
	for yidx in xrange(signal.shape[0]):
		for xidx in xrange(signal.shape[1]):
			if err[yidx][xidx]!=0:
				sign[yidx][xidx] = signal[yidx][xidx]/err[yidx][xidx]
			else:
				sign[yidx][xidx] = -1

	
	#-------------------------------#
	#								#
	#		PLOT SIGNIFICANCE 		#
	#								#
	#-------------------------------#
	# plot significance
	# LON AXIS
	region='ROI'
	n_lbins = int(np.abs(l1-l0)/lbin_size)
	lbins = functions.GenLonAxis_sym(l0, n_lbins, lbin_size, region)
	# COLORS, MARKERS
	colorlist, shape = functions.ColorAndMarker()
	# USE LATEX
	plt.rc('text', usetex=True)
	plt.rc('font', family='serif')
	# sign figure
	fig_sig=plt.figure()
	ax_sig=fig_sig.add_axes((.1,.1,0.8,0.8)) 	#left,bottom, width, height
	ax_sig.set_title("significance, E = (%.2f - %.2f)TeV, b=[%.2f, %.2f]$^\circ$" %(emin,emax,b0,b1))
	ax_sig.set_xlim(lbins[0]+.5, lbins[-1]-.5)
	ax_sig.grid(color=colorlist[-2], linestyle='--', linewidth='.5', alpha=0.25)
	ax_sig.set_ylabel('significance')
	ax_sig.set_xlabel("longitude [$^\circ$]")
	for bidx in xrange(int(np.abs(b1-b0)/bbin_size)):
		bmin = b0+bidx*bbin_size
		bmax = bmin + bbin_size
		ax_sig.plot(lbins, sign[bidx], color=colorlist[bidx], linestyle="", marker=shape[bidx], markersize='5', label="b-band: [%.1f,%.1f]$^\circ$" %(bmin, bmax))
	# location of legend
	ax_sig.legend(loc='best', ncol=1, frameon=True, framealpha=0.25)


	#---------------------------------------------------#
	#													#
	#		CUTS FOR SCA BUBBLES TEMPLATE IN ROI		#
	#													#
	#---------------------------------------------------#
	if use_iTemp==False:
		print('>>> Setup SCA Bubbles Template <<<')
		print('>>> Apply Template Cuts <<<')
		# set up FB template with certain cuts: threshold, reliability(signal!>mod+err)
		fb = np.zeros((fitdata.shape[1],fitdata.shape[2]))
		# define jump to the right (on region in lat = 1/2 * data in lat)
		xjump = fitdata.shape[2]/2
		while np.amax(fb)==0 and threshold>=0.:
			if threshold<0:
				print('--- choose a threshold > 0 ---')
				exit()
			for yidx in xrange(signal.shape[0]):
				for xidx in xrange(signal.shape[1]):
					# test if significance is above threshold
					if sign[yidx][xidx]>=threshold:
						# is the signal of the FB larger than mod+sca_err => set template px to zero
						if signal[yidx][xidx]>mod[yidx][xidx]+outlier_sigma*err[yidx][xidx]:
							fb[yidx][xidx+xjump]=0
						else:
							fb[yidx][xidx+xjump]=signal[yidx][xidx]#hard_on[yidx][xidx]
					else:
						pass

			#---------------------------#
			#							#
			#		NORMALIZATION		#
			#							#
			#---------------------------#
			# if no template entry exist - reduce threshold
			if np.amax(fb)==0:
				threshold-=1.

		print('--- threshold for sca template: %.2f ---' %threshold)
		ax_sig.axhline(y=threshold, xmin=0, xmax=1, color='r', linestyle=':', linewidth=2.)#, label='threshold')

	#-------------------------------------------------------------------#
	#																	#
	#		SCA BUBBLES TEMPLATE IN ROI BASED ON FERMI LAT TEMPLATE		#
	#																	#
	#-------------------------------------------------------------------#
	if use_iTemp==True:
		# used input template
		path2InTemp = 'FBmodel/input_map_fermi_bubbles.fits'
		print('--- used input template --- \n%s' %path2InTemp)
		# set the input template in the given ROI
		iTemp = InOutTemp.InputTemp(path2InTemp, 
			l0=l0, l1=l1, b0=b0, b1=b1, lbin_size=lbin_size, bbin_size=bbin_size)
		# apply cuts on fb template
		print('>>> Apply Template Cuts <<<')
		fb = np.zeros((fitdata.shape[1],fitdata.shape[2]))
		# define jump to the right (on region in lat = 1/2 * data in lat)
		xjump = fitdata.shape[2]/2
		while np.amax(fb)==0 and threshold>=0.:
			if threshold<0:
				print('--- choose a threshold > 0 ---')
				exit()
			for yidx in xrange(signal.shape[0]):
				for xidx in xrange(signal.shape[1]):
					# test if significance is above threshold
					if sign[yidx][xidx]>=threshold:
						# is the signal of the FB larger than mod+sca_err => set template px to zero
						if signal[yidx][xidx]>mod[yidx][xidx]+outlier_sigma*err[yidx][xidx]:
							fb[yidx][xidx+xjump]=0
						else:
							fb[yidx][xidx+xjump]=iTemp[yidx][xidx]#hard_on[yidx][xidx]
					else:
						pass
			# if no template entry exist - reduce threshold
			if np.amax(fb)==0:
				threshold-=1.
		print('--- threshold for sca template: %.2f ---' %threshold)
		ax_sig.axhline(y=threshold, xmin=0, xmax=1, color='r', linestyle=':', linewidth=2.)#, label='threshold')

	# set right px index to slice data
	b_low = functions.lat2px(b0, 0., new_yref, new_pxs)
	b_high = functions.lat2px(b1, 0., new_yref, new_pxs)
	l_left = functions.lon2px(360.-l1, 0., new_xref, new_pxs)
	l_right = functions.lon2px(l1, 0., new_xref, new_pxs)

	#-------------------------------#
	#								#
	#		RESIDUAL HARD TEMPLATE 	#
	#								#
	#-------------------------------#
	print('>>> Setup Residual Hard Template <<<')
	reshard_temp = np.zeros((fitdata.shape[1],fitdata.shape[2]))
	scadata = scadata[b_low:b_high,l_left:l_right]
	for i in xrange(fitdata.shape[1]):
		for j in xrange(fitdata.shape[2]):
			if fb[i][j]==0:
				reshard_temp[i][j] = scadata[i][j]
			else:
				reshard_temp[i][j] = scadata[i][j]-fb[i][j]

	#-----------------------#
	#						#
	#		BKG TEMPLATE 	#
	#						#
	#-----------------------#
	print('>>> Setup BKG Template <<<')
	# use the soft sca componant map
	bkg_temp = bkgdata[b_low:b_high,l_left:l_right]

	#-----------------------------------#
	#									#
	#		SAVE TEMPLATES TO FITS 		#
	#									#
	#-----------------------------------#
	# set filenames
	if 'Softer' not in modelpar.keys() and 'Harder' not in modelpar.keys():
		info_temp = '%isig_%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(threshold, sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
		name4Temp = 'sca_fb_temp_%s.fits' %info_temp
		name4reshardTemp = 'sca_hardbkg_temp_%s.fits' %info_temp
		name4bkgTemp = 'sca_softbkg_temp_%s.fits' %info_temp
	if 'Softer' not in modelpar.keys() and 'Harder' in modelpar.keys():
		info_temp = '%isig_%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(threshold, sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
		name4Temp = 'sca_fb_temp_%s.fits' %info_temp
		name4reshardTemp = 'sca_hardbkg_temp_%s.fits' %info_temp
		name4bkgTemp = 'sca_softbkg_temp_%s.fits' %info_temp
	if 'Harder' not in modelpar.keys() and 'Softer' in modelpar.keys():
		info_temp = '%isig_%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(threshold, sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
		name4Temp = 'sca_fb_temp_%s.fits' %info_temp
		name4reshardTemp = 'sca_hardbkg_temp_%s.fits' %info_temp
		name4bkgTemp = 'sca_softbkg_temp_%s.fits' %info_temp
	if 'Softer' in modelpar.keys() and 'Harder' in modelpar.keys():
		info_temp = '%isig_%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(threshold, sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
		name4Temp = 'sca_fb_temp_%s.fits' %info_temp
		name4reshardTemp = 'sca_hardbkg_temp_%s.fits' %info_temp
		name4bkgTemp = 'sca_softbkg_temp_%s.fits' %info_temp
	# set up correct reference px and deg
	# ref_x = -(360-l0-.5)

	# print FB template to fits file
	if use_iTemp==False:
		if functions.FileExist1('sca_temp/fit_'+name4Temp)==False:
			print('>>> PRINT NEW SCA BUBBLES TEMPLATE <<<')
			functions.Print2Fits(fb, new_pxs, fit_xref, fit_yref, 0., 0., 'sca_temp/fit_'+name4Temp)
		else:
			print('>>> TEMPLATE EXISTS ALREADY - REMOVE OLD TEMPLATE <<<')
			os.remove('sca_temp/fit_'+name4Temp)
			functions.Print2Fits(fb, new_pxs, fit_xref, fit_yref, 0., 0., 'sca_temp/fit_'+name4Temp)
	if use_iTemp==True:
		if functions.FileExist1('sca_temp/fit_input_fb_temp_%s.fits' %info_temp)==False:
			print('>>> PRINT NEW SCA BUBBLES TEMPLATE <<<')
			functions.Print2Fits(fb, new_pxs, fit_xref, fit_yref, 0., 0., 'sca_temp/fit_input_fb_temp_%s.fits' %info_temp)
		else:
			print('>>> TEMPLATE EXISTS ALREADY - REMOVE OLD TEMPLATE <<<')
			os.remove('sca_temp/fit_input_fb_temp_%s.fits' %info_temp)
			functions.Print2Fits(fb, new_pxs, fit_xref, fit_yref, 0., 0., 'sca_temp/fit_input_fb_temp_%s.fits' %info_temp)
	# print residual hard template to fits file
	if functions.FileExist1('sca_temp/bkg_model/fit_'+name4reshardTemp)==False:
		print('>>> PRINT NEW RESIDUAL HARD TEMPLATE <<<')
		functions.Print2Fits(reshard_temp, new_pxs, fit_xref, fit_yref, 0., 0., 'sca_temp/bkg_model/fit_'+name4reshardTemp)
	else:
		print('>>> RESIDUAL HARD TEMPLATE EXISTS ALREADY - REMOVE OLD TEMPLATE <<<')
		os.remove('sca_temp/bkg_model/fit_'+name4reshardTemp)
		functions.Print2Fits(reshard_temp, new_pxs, fit_xref, fit_yref, 0., 0., 'sca_temp/bkg_model/fit_'+name4reshardTemp)
	# print BKG template to fits file
	if functions.FileExist1('sca_temp/bkg_model/fit_'+name4bkgTemp)==False:
		print('>>> PRINT NEW BKG TEMPLATE <<<')
		functions.Print2Fits(bkg_temp, new_pxs, fit_xref, fit_yref, 0., 0., 'sca_temp/bkg_model/fit_'+name4bkgTemp)
	else:
		print('>>> BKG TEMPLATE EXISTS ALREADY - REMOVE OLD TEMPLATE <<<')
		os.remove('sca_temp/bkg_model/fit_'+name4bkgTemp)
		functions.Print2Fits(bkg_temp, new_pxs, fit_xref, fit_yref, 0., 0., 'sca_temp/bkg_model/fit_'+name4bkgTemp)
		

	#-------------------------------#
	#								#
	#		PLOT SCA TEMPLATEs 		#
	#								#
	#-------------------------------#
	# plot SCA Bubbles Template
	fig_fb = plt.figure()
	ax_fb=fig_fb.add_axes((.1,.1,0.8,0.8)) 	#left,bottom, width, height
	if use_iTemp==True:
		ax_fb.set_title('Input Bubbles Template, threshold = %.1f$\sigma$' %threshold)
	else:
		ax_fb.set_title('SCA Bubbles Template, threshold = %.1f$\sigma$' %threshold)
	ax_fb.set_xlabel("longitude [$^\circ$]")
	ax_fb.set_ylabel("latitude [$^\circ$]")
	temp_fb = plt.imshow(fb/np.amax(fb), origin="lower", extent=(360-l1, -(360-l1), b0, b1))
	cbar_fb = plt.colorbar(temp_fb, orientation='horizontal')
	# cbar_fb.set_label("sr$^{-1}$")
	# cbar_fb.set_label("ph. counts")

	# plot residual hard Template
	fig_hard = plt.figure()
	ax_hard=fig_hard.add_axes((.1,.1,0.8,0.8)) 	#left,bottom, width, height
	ax_hard.set_title('Residual Hard Component Template')
	ax_hard.set_xlabel("longitude [$^\circ$]")
	ax_hard.set_ylabel("latitude [$^\circ$]")
	temp_hard = plt.imshow(reshard_temp/np.amax(reshard_temp), origin="lower", extent=(360-l1, -(360-l1), b0, b1))
	cbar_hard = plt.colorbar(temp_hard, orientation='horizontal')
	# cbar_hard.set_label("sr$^{-1}$")
	# cbar_hard.set_label("ph. counts")

	# plot SCA BKG Template
	fig_soft = plt.figure()
	ax_soft=fig_soft.add_axes((.1,.1,0.8,0.8)) 	#left,bottom, width, height
	ax_soft.set_title('Soft Component Template')
	ax_soft.set_xlabel("longitude [$^\circ$]")
	ax_soft.set_ylabel("latitude [$^\circ$]")
	temp_soft = plt.imshow(bkg_temp/np.amax(bkg_temp), origin="lower", extent=(360-l1, -(360-l1), b0, b1))
	cbar_soft = plt.colorbar(temp_soft, orientation='horizontal')
	# cbar_soft.set_label("sr$^{-1}$")
	# cbar_soft.set_label("ph. counts")


	#-----------------------------------#
	#									#
	#		SAVE & SHOW RESULTS 		#
	#									#
	#-----------------------------------#
	if save_results==True:
		print('>>> SAVE RESULTS <<<')
		if finalplots==True:
			fig_fb.savefig(path2results+'/fit_fbtemp_%s.pdf' %info_temp)
			fig_hard.savefig(path2results+'/fit_rhardtemp_%s.pdf' %info_temp)
			fig_soft.savefig(path2results+'/fit_bkgtemp_%s.pdf' %info_temp)
			fig_sig.savefig(path2results+'/sign_%s.pdf' %info_temp)
		else:
			fig_fb.savefig(path2results+'/fit_fbtemp_%s.png' %info_temp)
			fig_hard.savefig(path2results+'/fit_rhardtemp_%s.png' %info_temp)
			fig_soft.savefig(path2results+'/fit_bkgtemp_%s.png' %info_temp)
			fig_sig.savefig(path2results+'/sign_%s.png' %info_temp)

	if show_results==True:
		print('>>> PLOT RESULTS <<<')
		plt.show()
	if show_results==False:
		print('>>> CLOSE FIGURES <<<')
		plt.close('all')

	if np.amax(fb)==0:
		print('\n!>!>!>!>! NO FERMI BUBBLES DETECTED !<!<!<!<!\n')
		exit()

	print('>>> Compare Input/Output FB Templates <<<')
	if use_iTemp==False:
		CompareInOutTemp(path2InTemp='FBmodel/input_map_fermi_bubbles.fits', path2outTemp='sca_temp/fit_'+name4Temp,
			l0=l0, l1=l1, b0=b0, b1=b1, lbin_size=lbin_size, bbin_size=bbin_size,
			path2results=path2results, show_results=show_results, save_results=save_results, finalplots=finalplots)
	else:
		CompareInOutTemp(path2InTemp='FBmodel/input_map_fermi_bubbles.fits', path2outTemp='sca_temp/fit_input_fb_temp_%s.fits' %info_temp,
			l0=l0, l1=l1, b0=b0, b1=b1, lbin_size=lbin_size, bbin_size=bbin_size,
			path2results=path2results, show_results=show_results, save_results=save_results, finalplots=finalplots)

	return threshold



#==================================================================================================================================
#==================================================================================================================================
#==================================================================================================================================




def CompareInOutTemp(path2InTemp, path2outTemp,
	l0=360., l1=355., b0=-5., b1=-1., lbin_size=.5, bbin_size=.5,
	path2results='', show_results=False, save_results=False, finalplots=False):

	# get info about the template from input string
	def substring_after(s, delim):
		return s.partition(delim)[2]
	info_temp = substring_after(path2outTemp, 'temp_')

	iTemp = InOutTemp.InputTemp(path2InTemp,
		l0=l0, l1=l1, b0=b0, b1=b1, lbin_size=lbin_size, bbin_size=bbin_size,)
	oTemp = InOutTemp.OutputTemp(path2outTemp,
		l0=l0, l1=l1, b0=b0, b1=b1, lbin_size=lbin_size, bbin_size=bbin_size,)

	fig_in = plt.figure()
	fig_out = plt.figure()
	ax1 = fig_in.add_axes((0.1,0.1,0.8,0.8))
	ax2 = fig_out.add_axes((0.1,0.1,0.8,0.8))

	ax1.set_title('Input Template')
	ax1.set_ylabel('latitude [$^\circ$]')
	ax1.set_xlabel('longitude [$^\circ$]')
	ax2.set_title('Output Template')
	ax2.set_ylabel('latitude [$^\circ$]')
	ax2.set_xlabel('longitude [$^\circ$]')

	r1 = ax1.imshow(iTemp, origin='lower', extent=[0., -(360-l1), b0, b1])
	r2 = ax2.imshow(oTemp, origin='lower', extent=[0., -(360-l1), b0, b1])

	cbar_in = fig_in.colorbar(r1,orientation='horizontal')
	cbar_out = fig_out.colorbar(r2,orientation='horizontal')

	res_Temp = np.zeros_like(iTemp)
	for i in xrange(iTemp.shape[0]):
		for j in xrange(iTemp.shape[1]):
			res_Temp[i][j] = (oTemp[i][j]-iTemp[i][j])/iTemp[i][j]


	fig_res=plt.figure()
	ax_res=fig_res.add_axes((0.1,0.1,0.8,0.8))
	
	ax_res.set_title('Deviation from Input Template in the ROI')
	ax_res.set_xlabel('longitude [$^\circ$]')
	ax_res.set_ylabel('latitude [$^\circ$]')
	res = ax_res.imshow(res_Temp, origin='lower', extent=(0., -(360-l1), b0, b1))
	cbar = plt.colorbar(res,orientation='horizontal')
	# cbar.set_label('$\%$')

	if save_results==True:
		print('>>> SAVE RESULTS <<<')
		if finalplots==True:
			fig_in.savefig(path2results+'/ROI_itemp_%s.pdf' %info_temp)
			fig_out.savefig(path2results+'/ROI_otemp_%s.pdf' %info_temp)
			fig_res.savefig(path2results+'/ROI_restemp_%s.pdf' %info_temp)
		else:
			fig_in.savefig(path2results+'/ROI_itemp_%s.png' %info_temp)
			fig_out.savefig(path2results+'/ROI_otemp_%s.png' %info_temp)
			fig_res.savefig(path2results+'/ROI_restemp_%s.png' %info_temp)

	if show_results==True:
		print('>>> PLOT RESULTS <<<')
		plt.show()
	if show_results==False:
		print('>>> CLOSE FIGURES <<<')
		plt.close('all')






#==================================================================================================================================
#==================================================================================================================================
#==================================================================================================================================


def plt_resmap(path2resmap, b=-1):
	resmap = pf.open(path2resmap)[0].data
	plt.title('Residual Map')
	plt.xlabel('longitude [deg]')
	plt.ylabel('latitude [deg]')
	plt.imshow(resmap, origin='lower', extent=[5,-5,-5,-1])
	plt.colorbar(orientation='horizontal')
	plt.show()


def RecoSpec(threshold=5., sim_type=sim_type, exrdef=exrdef, modelpar=modelpar, irf=irf,
	ellim=0.03, Emin=E_min, eulim=160., Emax=E_max, pxs=pxs, new_pxs=new_pxs, new_xref=1., new_yref=1., t_obs=t_obs, use_iTemp=False,
	path2results=path2results, save_results=save_results, show_results=show_results, finalplots=finalplots):
	#INPUT
	#OUTPUT

	#-----------------------------------------------------------#
	#															#
	#		SET FILE NAMES NEEDED FOR THE CTLIKE/CSSPEC 		#
	#															#
	#-----------------------------------------------------------#
	PATH = os.getenv('OWNSIM')
	# set response_info
	res_info = '%s_%s_%.2fE%.2f_%iEbins_%.2fpxs_t%i' %(irf[1],irf[0],Emin,Emax,Ebins,pxs,t_obs)

	# set temp_info
	if 'Softer' not in modelpar.keys() and 'Harder' not in modelpar.keys():
		info_temp = '%isig_%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(threshold, sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
		info_cube = '%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
	if 'Softer' not in modelpar.keys() and 'Harder' in modelpar.keys():
		info_temp = '%isig_%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(threshold, sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
		info_cube = '%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
	if 'Harder' not in modelpar.keys() and 'Softer' in modelpar.keys():
		info_temp = '%isig_%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(threshold, sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
		info_cube = '%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
	if 'Softer' in modelpar.keys() and 'Harder' in modelpar.keys():
		info_temp = '%isig_%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(threshold, sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
		info_cube = '%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)

	# name for hard sca component map
	name4scahard = 'hard_skymap_%s.fits' %info_cube
	if functions.FileExist1('skymaps/'+name4scahard)==False:
		print('>>> no hard skymap found <<<')
		print name4scahard
		exit()
	# name for soft sca component map
	name4scasoft = 'soft_skymap_%s.fits' %info_cube
	if functions.FileExist1('skymaps/'+name4scasoft)==False:
		print('>>> no soft skymap found <<<')
		print name4scasoft
		exit()
	# name of the cube used for ctlike
	name4cube = 'cube4fit_%s.fits' %info_cube
	if functions.FileExist1('sca_data/'+name4cube)==False:
		print('>>> no cube4fit found <<<')
		print name4cube
		exit()
	# name of the sca fermi bubbles template
	name4Temp = 'sca_fb_temp_%s.fits' %info_temp
	if functions.FileExist1('sca_temp/'+name4Temp)==False:
		print('>>> no sca_fb_temp found <<<')
		print name4Temp
		exit()
	# name of the residual hard template
	name4reshardTemp = 'sca_hardbkg_temp_%s.fits' %info_temp
	if functions.FileExist1('sca_temp/bkg_model/'+name4reshardTemp)==False:
		print('>>> no sca_hardbkg_temp found <<<')
		print name4reshardTemp
		exit()
	# name of the bkg template
	name4bkgTemp = 'sca_softbkg_temp_%s.fits' %info_temp
	if functions.FileExist1('sca_temp/bkg_model/'+name4bkgTemp)==False:
		print('>>> no sca_softbkg_temp found <<<')
		print name4bkgTemp
		exit()
	# name of the xml model for the ExpCutOff FB
	# CTADATA = os.getenv('CTADATA')
	# name4fbmod = CTADATA+'/models/models_gc_ExpCut_FermiBubbles_noBKG.xml'
	# name4fbmod = 'sca_model.xml'
	name4fbmod = 'sca_fb_unknown_ext.xml'
	# name4fbmod = 'fermi_bubbles_ExpCutOff.xml'
	if functions.FileExist1('FBmodel/'+name4fbmod)==False:
		print('>>> no FB model found <<<')
		print name4fbmod
		exit()
	# name of the output model for the FB
	name4outmod = 'mod_%s.xml' %info_temp

	# solid angle
	dO = functions.dOmega(new_pxs)

	print('used model xml file:\n%s' %name4fbmod)

	#-------------------------------------------------------------------------------#
	#																				#
	#		SET THE USED TEMPLATES TO THE MODEL XML FILE USED BY CTLIKE/CSSPEC 		#
	#																				#
	#-------------------------------------------------------------------------------#
	# use the sca template to calc spectrum
	if use_iTemp==False:
		print('--- use sca templates for FB & BKG ---')
		# set correct template file to xml model file fermi_bubbles_ExpCutOff to fit right data and calc correct spectrum
		# load xml file
		tree = et.parse('FBmodel/'+name4fbmod)
		# tree = et.parse(name4fbmod)
		root = tree.getroot()
		# find sources in xml file
		for s in root.findall('source'):
			if s.attrib['name']=='Fermi bubbles':
				# write the right sca bubbles template to the model xml used by ctlike
				s.find('spatialModel').attrib['file']='sca_temp/'+name4Temp
				for spec in s.findall('spectrum'):
					# use the hard index for the FB model
					for par in spec.findall('parameter'):
						if par.attrib['name']=='Index':
							par.attrib['value']=str(-modelpar['Hard'][3])
			# if s.attrib['name']=='HARDBKG':
			# 	# write the right sca bubbles template to the model xml used by ctlike
			# 	s.find('spatialModel').attrib['file']='sca_temp/bkg_model/'+name4reshardTemp
			# 	for spec in s.iter('spectrum'):
			# 		# use the hard index for the FB model
			# 		for par in spec.iter('parameter'):
			# 			if par.attrib['name']=='Index':
			# 				par.attrib['value']=str(-modelpar['Hard'][3])
			# if s.attrib['name']=='SOFTBKG' and 'sca' in name4fbmod:
			# 	# write the right sca bkg template to the model xml used by ctlike
			# 	s.find('spatialModel').attrib['file']='sca_temp/bkg_model/'+name4bkgTemp
			# 	for spec in s.findall('spectrum'):
			# 		# use the soft index for the bkg model
			# 		for par in spec.findall('parameter'):
			# 			if par.attrib['name']=='Index':
			# 				par.attrib['value']=str(-modelpar['Soft'][3])
		# write change to xml file
		tree.write('FBmodel/'+name4fbmod)
		# tree.write(name4fbmod)

	# use the input template in the ROI to calc the spectrum
	if use_iTemp==True:
		print('--- use input template ---')
		# type in the parameter for On-region
		print('--- initialize the ROI (l0,l1), (b0,b1) ---')
		l0 = input('left bound l0 [deg], float ')
		l1 = input('right bound l1 [deg], float ')
		b0 = input('lower bound b0 [deg], float ')
		b1 = input('upper bound b1 [deg], float ')
		print('--- initialize the pxs in longitude/latitude=lbin_size/bbin_size ---')
		lbin_size = input('lbin_size [deg], float ')
		bbin_size = input('bbin_size [deg], float ')
		# used input template
		path2InTemp = 'FBmodel/input_map_fermi_bubbles.fits'
		print('--- used input template --- \n%s' %path2InTemp)
		# set the input template in the given ROI
		InOutTemp.InputTemp(path2InTemp, 
			l0=l0, l1=l1, b0=b0, b1=b1, lbin_size=lbin_size, bbin_size=bbin_size)

		# set correct template file to xml model file fermi_bubbles_ExpCutOff to fit right data and calc correct spectrum
		# load xml file
		tree = et.parse('FBmodel/'+name4fbmod)
		root = tree.getroot()
		# find spatial model in xml file
		ROIinfo = '%.2fl%.2f_%.2fb%.2f_%.2fpxs' %(l0,l1,b0,b1,new_pxs)
		name4iTemp = 'input_temp_%s.fits' %ROIinfo
		for s in root.iter('spatialModel'):
			# change file attribute in xml file
			s.attrib['file'] = 'sca_temp/'+name4iTemp	#using the input template in ROI
		# write change to xml file
		tree.write('FBmodel/'+name4fbmod)

	# print name4fbmod
	#-----------------------#
	#						#
	#		RUN CTLIKE/		#
	#						#
	#-----------------------#
	print('>>> Fit SCA Model to Data <<<')
	# data to get infos
	fitdata, fheader = functions.LoadFitsData('sca_data/'+name4cube, 0)
	# Perform maximum likelihood fitting
	like = ctools.ctlike()
	like['inobs']    = 'sca_data/'+name4cube
	like['inmodel']  = 'FBmodel/'+name4fbmod
	# like['inmodel']  = name4fbmod
	like['outmodel'] = path2results+'/'+name4outmod
	like['expcube']  = PATH+'/response/expcube_%s.fits' %res_info
	like['psfcube']  = PATH+'/response/psfcube_%s.fits' %res_info
	like['bkgcube']  = PATH+'/response/bkgcube_%s.fits' %res_info
	like['caldb']    = irf[1]
	like['irf']      = irf[0]
	like['debug']    = False # Switch this always on for results in console
	like.execute()

	print like.opt()

	# run csresmap
	resmap = cscripts.csresmap(like.obs())
	resmap['outmap']	= 'resmap.fits'
	resmap['algorithm'] = 'SUB'
	resmap['emin']      = ellim
	resmap['emax']      = eulim
	resmap['nxpix']     = fitdata.shape[2]
	resmap['nypix']     = fitdata.shape[1]
	resmap['binsz']     = new_pxs
	resmap['proj']      = 'CAR'
	resmap['coordsys']  = 'GAL'
	resmap['xref']      = 0.
	resmap['yref']      = 0.
	resmap.execute()

	plt_resmap('resmap.fits')
	res_ok = input('OK? [True|False] ')
	while res_ok==False:
		print('... add new Point Source:')
		# add new src and type parameter to console
		lon = input('guessed longitude [deg] ')
		lat = input('guessed latitude [deg] ')
		flux = input('guessed flux [ph/MeV/cm^2/s] ')
		ra, dec = functions.gal2icrs(lon, lat)
		index = input('spectral index [+/-float] ')
		newpntsrc = gammalib.GModelSky(gammalib.GModelSpatialPointSource(ra,dec),
                               gammalib.GModelSpectralPlaw(flux,index,gammalib.GEnergy(1.e3,'MeV')),
                               gammalib.GModelTemporalConst(1))
		name4src = input('src name [string] ')
		newpntsrc.name(name4src)
		# set the parameter to free
		# newpntsrc['RA'].free()
		# newpntsrc['DEC'].free()
		# add new src to models
		like.obs().models().append(newpntsrc)
		# run ctlike again
		like.execute()
		# run csresmap again
		resmap.execute()
		plt_resmap('resmap.fits')
		# is the resmap ok?
		res_ok = input('OK? [True|False] ')

	print('>>> Calc. SCA Bubbles Spectrum <<<')
	# Calculate spectrum
	outspecfile = 'FBmodel/outspectrum/sca_fb_spec_%s.fits' %info_temp
	spec = cscripts.csspec()
	spec['inobs'] = 'sca_data/'+name4cube
	spec['inmodel'] = path2results+'/'+name4outmod
	spec['srcname'] = 'Fermi bubbles'
	spec['expcube'] = PATH+'/response/expcube_%s.fits' %res_info
	spec['psfcube'] = PATH+'/response/psfcube_%s.fits' %res_info
	spec['bkgcube'] = PATH+'/response/bkgcube_%s.fits' %res_info
	spec['caldb'] = irf[1]
	spec['irf'] = irf[0]
	spec['ebinalg'] = 'FILE'
	spec['ebinfile'] = 'sca_data/'+name4cube
	spec['outfile'] = outspecfile
	spec.execute()


	#-----------------------------------#
	#									#
	#		PLOT IN/OUT SPECTRUM 		#
	#									#
	#-----------------------------------#
	# load color and marker shape
	colorlist, shape = functions.ColorAndMarker()
	# set plot figure
	spectrum = plt.figure()
	plt.loglog()
	plt.grid(color=colorlist[-2], linestyle='--', linewidth='0.5')
	plt.title('Fermi Bubbles Spectrum, E =  (%.2f - %.2f)TeV' %(ellim, eulim))
	plt.xlabel('Energy [TeV]')
	plt.ylabel(r'E$^2$ $\times$ dN/dE [GeV cm$^{-2}$ s$^{-1}$]')
	# plt.ylim(1e-9,1e-7)

	# Read spectrum file    
	fits     = gammalib.GFits(outspecfile)
	table    = fits.table(1)
	c_energy = table['Energy']
	c_ed     = table['ed_Energy']
	c_eu     = table['eu_Energy']
	c_flux   = table['Flux']
	c_eflux  = table['e_Flux']
	c_ts     = table['TS']
	c_upper  = table['UpperLimit']

	# Initialise arrays to be filled
	energies    = []
	flux        = []
	ed_engs     = []
	eu_engs     = []
	e_flux      = []
	ul_energies = []
	ul_ed_engs  = []
	ul_eu_engs  = []
	ul_flux     = []

	erg2GeV = 624.151
	TeV2GeV = 1e3

	#-------------------#
	#  Fitted Spectrum  #
	#-------------------#
	# Loop over rows of the file
	nrows = table.nrows()
	for row in range(nrows):

		# Get Test Statistic, flux and flux error
		ts    = c_ts.real(row)
		flx   = c_flux.real(row)
		e_flx = c_eflux.real(row)

		# If Test Statistic is larger than 9 and flux error is smaller than
		# flux then append flux plots ...
		if ts > 9.0 and e_flx < flx:
			energies.append(c_energy.real(row))
			flux.append(c_flux.real(row)*erg2GeV)
			ed_engs.append(c_ed.real(row))
			eu_engs.append(c_eu.real(row))
			e_flux.append(c_eflux.real(row))

		# ... otherwise append upper limit
		else:
			ul_energies.append(c_energy.real(row))
			ul_flux.append(c_upper.real(row)*erg2GeV)
			ul_ed_engs.append(c_ed.real(row))
			ul_eu_engs.append(c_eu.real(row))

	# Plot the spectrum 
	plt.errorbar(energies, flux, yerr=e_flux, xerr=[ed_engs, eu_engs], color=colorlist[1], linestyle='', marker='o')
	plt.errorbar(ul_energies, ul_flux, xerr=[ul_ed_engs, ul_eu_engs], yerr=1.0e-11, uplims=True, color=colorlist[1], linestyle='', marker='o')

	#------------------#
	#  Model Spectrum  #
	#------------------#
	# energy as np.array from now table
	energy = np.zeros(nrows)
	for r in xrange(nrows):
		energy[r] = c_energy.real(r)

	#=== Set up simulated ExpCutOff FB ===#
	E0=2.       #[GeV]
	E_cut=5e4   #[GeV]
	k0=1e-6     #[ph/GeV/cm2/s/sr]
	index=-2
	FB = (energy*TeV2GeV)**2 * functions.ExpCutOffPowerLaw(energy*TeV2GeV, E0, E_cut, k0, index) #[GeV/cm2/s/sr]

	# number of px used in the FB template
	fb_temp = pf.open('sca_temp/'+name4Temp)[0].data
	npx=np.count_nonzero(fb_temp)
	# flux in all px
	FB_flux = FB*dO

	# Plt the spectrum
	plt.plot(energy, FB_flux, color=colorlist[-1], linestyle='--', label='expected')

	#-----------------------------------#
	#									#
	#		SAVE & SHOW RESULTS 		#
	#									#
	#-----------------------------------#
	if save_results==True:
		if finalplots==True:
			spectrum.savefig(path2results+'/spec_%s.pdf' %info_temp)
		else:
			spectrum.savefig(path2results+'/spec_%s.png' %info_temp)

	if show_results==True:
		print('>>> PLOT RESULTS <<<')
		plt.show()
	if show_results==False:
		print('>>> CLOSE FIGURES <<<')
		plt.close('all')

	return 0





#==================================================================================================================================
#==================================================================================================================================
#==================================================================================================================================





# def CalcSigma2(sig2=1., xyref=10.5,
# 			sim_type=sim_type, exrdef=exrdef, modelpar=modelpar, irf=irf, emin=emin, emax=emax, new_pxs=new_pxs, t_obs=t_obs):
# 	#INPUT
# 	#OUTPUT

# 	# load F matrix from yaml file (stored by applying SCA.py)
# 	if 'Softer' not in modelpar.keys() and 'Harder' not in modelpar.keys():
# 		fn = 'sca_error/Fmatrix_%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.yaml' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
# 	if 'Softer' not in modelpar.keys() and 'Harder' in modelpar.keys():
# 		fn = 'sca_error/Fmatrix_%s_%s_Hard_%.2f_Harder_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.yaml' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Harder'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
# 	if 'Harder' not in modelpar.keys() and 'Softer' in modelpar.keys():
# 		fn = 'sca_error/Fmatrix_%s_%s_Hard_%.2f_Soft_%.2f_Softer_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.yaml' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], modelpar['Softer'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
# 	if 'Softer' in modelpar.keys() and 'Harder' in modelpar.keys():
# 		fn = 'sca_error/Fmatrix_%s_%s_Hard_%.2f_Harder_%.2f_Soft_%.2f_Softer_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.yaml' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Harder'][3], modelpar['Soft'][3], modelpar['Softer'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
# 	if functions.FileExist1(fn)==True:
# 		F = np.asarray(dio.loaddict(fn)['F'])
# 	else:
# 		print('>>> no yaml file for the F matrix found <<<')
# 		print fn
# 		exit()

# 	#Path to sigma2
# 	path2sgima2 = 'sca_error/'
# 	if 'Softer' not in modelpar.keys() and 'Harder' not in modelpar.keys():
# 		name4sgima2 = 'sigma2_%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.fits' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
# 	if 'Softer' not in modelpar.keys() and 'Harder' in modelpar.keys():
# 		name4sgima2 = 'sigma2_%s_%s_Hard_%.2f_Harder_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.fits' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Harder'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
# 	if 'Harder' not in modelpar.keys() and 'Softer' in modelpar.keys():
# 		name4sgima2 = 'sigma2_%s_%s_Hard_%.2f_Soft_%.2f_Softer_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.fits' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], modelpar['Softer'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
# 	if 'Softer' in modelpar.keys() and 'Harder' in modelpar.keys():
# 		name4sgima2 = 'sigma2_%s_%s_Hard_%.2f_Harder_%.2f_Soft_%.2f_Softer_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i.fits' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Harder'][3], modelpar['Soft'][3], modelpar['Softer'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)

	
# 	# calc error
# 	# set npx to (re)shape the arrays
# 	FOV=10
# 	npx = int(FOV/new_pxs)
# 	# bring everything in the right form
# 	F_trans = F.T
# 	F_inv = np.zeros_like(F_trans)
# 	sca_sigma2 = np.zeros((sig2.shape[1], sig2.shape[0], F_trans.shape[1]))
# 	# loop over px and energy
# 	for pidx in xrange(F_trans.shape[0]):
# 		for eidx in xrange(sig2.shape[0]):
# 			if np.linalg.det(F_trans[pidx])==0:
# 				sca_sigma2[pidx][eidx][:]=0
# 				continue
# 			else:
# 				F_inv[pidx] = np.linalg.inv(F_trans[pidx])
# 			sca_sigma2[pidx][eidx] = np.diag(F_inv[pidx])*np.diag(F_trans[pidx])*sig2[eidx][pidx]

# 	sca_sigma2 = np.sum(sca_sigma2, axis=1)

# 	#print sca sigma^2 map
# 	sca_sigma2 = np.reshape(sca_sigma2, (npx, npx, sca_sigma2.shape[1])).T
# 	sca_sigma2 = sca_sigma2[0].T
# 	if functions.FileExist2(path2sgima2, name4sgima2)==False:
# 		print('>>> print new sigma^2 sky <<<')
# 		functions.Print2Fits(sca_sigma2, new_pxs, xyref, xyref, 0., 0., path2sgima2+name4sgima2)
# 	elif functions.FileExist2(path2sgima2, name4sgima2)==True:
# 		print('>>> sigma^2 sky exists already - flush sigma^2 <<<')
# 		errorsky = pf.open(path2sgima2+name4sgima2, mode='update')
# 		errorsky[0].data = sca_sigma2
# 		errorsky.flush()


if __name__ == '__main__':

	template = ApplyScaTemp(show_results=show_results, save_results=save_results, finalplots=finalplots)
	# RecoSpec()



