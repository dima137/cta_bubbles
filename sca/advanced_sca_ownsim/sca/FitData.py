import os
import shutil
import ctools
import cscripts
import numpy as np
import pyfits as pf
import matplotlib.pyplot as plt
import xml.etree.ElementTree as et
from scipy.optimize import curve_fit
#--- own modules ---#
import dio
import functions
import OnOffMethod

#-------------------------------------------------------------------------
#
#-------------------------------------------------------------------------

#---- LIST OF CONSIDERED TEMPLATES ----
temp_dct={'fb':True, 'hard':True, 'soft':True}	#{'fb':True, 'hard':True|False, 'soft':True|False}
												#{'fb':True, 'hard':True|False}
												#{'fb':True}
#---- IRF ---- 
#irf = ["South_z20_average_50h", "prod3b-v1"]		#["irf", "caldb"]=["South_z20_average_50h", "prod3b-v1"] or ["South_z20_50h", "1dc"]
irf = ["South_z20_50h", "1dc"]
#---- TYPE OF SIMULATION ----
sim_type = "gc_ExpCut_FermiBubbles_noBKG"	#[gc_noFB_noBKG | gc_ExpCut_FermiBubbles_noBKG | gc_ExpCut_FermiBubbles]
#----CHOOSE EXR DEFINITION----
exrdef = "exr_2deg_stripe"	#[exr_1deg_stripe | exr_2deg_stripe]
#----SET MODELS----
# k0, E0, E_cut, index
modelpar = {
	# 'Softer':[1., 1., 1.e30, -3.],
	'Soft':[1., 1., 1.e30, -2.6],
	'Hard':[1., 1., 5.e7, -2.],
	# 'Harder':[1., 1., 1.e30, -1.],
	}


def CalcMatrices(data, templates, sigma2):
	#INPUT:
		# data: np.array [nebins, npx]
		# templates: np.array [ntemplates, npx]
		# sigma2: sigma2 in each ebin, np.array [nebins, npx]

	#---------------------------#
	#	SETUP SOME PARAMETERS	#
	#---------------------------#
	nebins = data.shape[0]
	npx = data.shape[1]
	ntemp = templates.shape[0]

	#-------------------#
	#	CALC D MATRIX	#
	#-------------------#
	D = np.zeros((nebins, ntemp))
	# calc D matrix
	D = np.dot(data, templates.T)	
	# # calc data divided by sigma^2 in each energy bin
	# for e in xrange(nebins):
	# 	D[e] = d[e]/sigma2[e]

	#-------------------#
	#	CALC M MATRIX	#
	#-------------------#
	M = np.zeros((nebins, ntemp, ntemp))
	# calc M matrix
	for e in xrange(nebins):
		M[e] = np.dot(templates, templates.T)#/sigma2[e]

	return D,M

def RunFit(D, M):
	#INPUT
		# D: from CalcMatrices
		# M: from CalcMatrices

	#---------------------------#
	#	SETUP SOME PARAMETERS	#
	#---------------------------#
	nebins = D.shape[0]
	ntemp = D.shape[1]

	#-----------#
	#	FIT 	#
	#-----------#
	# fit templates to data in each energy bin
	F = np.zeros((nebins, ntemp))
	for e in xrange(nebins):
		if np.linalg.det(M[e])==0:
			print('--- Matrix not Invertable ---')
			exit()
		else:
			F[e] = np.dot(D[e], np.linalg.inv(M[e]))
			# F[e] = np.dot(np.linalg.inv(M[e]),D[e])
			# F[e] = np.linalg.solve(M[e], D[e])

	return F


def Fit2Data(threshold=1, temp_dct=temp_dct, fitted_erange=[1.,10.], use_iTemp=False, use_sca_ibkg=False,
	sim_type=sim_type, exrdef=exrdef, modelpar=modelpar, irf=irf,
	pxs=0.02, Emin=0.03, Emax=160., nEbins=20, t_obs=180000,
	new_pxs=.5, emin=.11, emax=18.72, new_xref=10.5, new_yref=10.5, fit_xref=6.5, fit_yref=2.5,
	b0=-3., b1=-1., l0=360., l1=357.,
	path2results='', show_results=True, save_results=False, finalplots=False):

	# use LATEX for plotting
	plt.rc('text', usetex=True)
	plt.rc('font', family='serif')

	#-----------------------#
	#	USEFUL PARAMETERS	#
	#-----------------------#
	MeV2TeV = 1e-6
	MeV2GeV = 1e-3

	#-----------------------#
	#	SETUP FILENAMES		#
	#-----------------------#
	# set path to $OWNISM
	PATH = os.getenv('OWNSIM')
	# set temp_info
	info_itemp = '%.2fl%.2f_%.2fb%.2f_%.2fpxs' %(l0,l1,b0,b1,new_pxs)
	if 'Softer' not in modelpar.keys() and 'Harder' not in modelpar.keys():
		info_temp = '%isig_%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(threshold, sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
		info_cube = '%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
	if 'Softer' not in modelpar.keys() and 'Harder' in modelpar.keys():
		info_temp = '%isig_%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(threshold, sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
		info_cube = '%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
	if 'Harder' not in modelpar.keys() and 'Softer' in modelpar.keys():
		info_temp = '%isig_%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(threshold, sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
		info_cube = '%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
	if 'Softer' in modelpar.keys() and 'Harder' in modelpar.keys():
		info_temp = '%isig_%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(threshold, sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)
		info_cube = '%s_%s_Hard_%.2f_Soft_%.2f_%s_%s_pxs%.2f_%.2fE%.2f_t%i' %(sim_type, exrdef, modelpar['Hard'][3], modelpar['Soft'][3], irf[1], irf[0], new_pxs, emin, emax, t_obs)

	# name of the cube used for fitting
	path2data = 'sca_data/cube4fit_%s.fits' %info_cube
	if functions.FileExist1(path2data)==False:
		print('>>> no cube4fit found <<<')
		print path2data
		exit()
	if use_iTemp==True:
		# name of the fermi bubbles input template
		path2fbTemp = 'FBmodel/input_map_fermi_bubbles.fits'
		if functions.FileExist1(path2fbTemp)==False:
			print('>>> no input_fb_temp found <<<')
			print path2fbTemp
			exit()
	else:
		# name of the sca fermi bubbles template
		path2fbTemp = 'sca_temp/fit_sca_fb_temp_%s.fits' %info_temp
		if functions.FileExist1(path2fbTemp)==False:
			print('>>> no sca_fb_temp found <<<')
			print path2fbTemp
			exit()

	# name of the sca hard component bkg template
	path2hardTemp = 'sca_temp/bkg_model/fit_sca_hardbkg_temp_%s.fits' %info_temp
	if functions.FileExist1(path2hardTemp)==False:
		print('>>> no sca_hardbkg_temp found <<<')
		print path2hardTemp
		exit()
	# name of the sca soft component bkg template
	path2softTemp = 'sca_temp/bkg_model/fit_sca_softbkg_temp_%s.fits' %info_temp
	if functions.FileExist1(path2softTemp)==False:
		print('>>> no sca_softbkg_temp found <<<')
		print path2softTemp
		exit()

	#---------------#
	#	LOAD DATA	#
	#---------------#
	# solid angle of one px
	dO = functions.dOmega(new_pxs)
	# load data from fits
	data, d_header = functions.LoadFitsData(path2data,0)
	energy, dE = functions.CalcE(path2data,2)
	fb_temp, fb_header = functions.LoadFitsData(path2fbTemp,0)
	hard_temp, h_header = functions.LoadFitsData(path2hardTemp,0)
	soft_temp, s_header = functions.LoadFitsData(path2softTemp,0)

	# get ebin index corresponding to the fitted energy range parameters
	list_fit_ebins = []
	for eidx, e in enumerate(energy):
		if fitted_erange[0]<=e*MeV2TeV and fitted_erange[1]>=e*MeV2TeV:
			list_fit_ebins.append(eidx)
	# setup arrays to these ebins
	data = data[list_fit_ebins[0]:list_fit_ebins[-1]+1,:,:]
	energy = energy[list_fit_ebins[0]:list_fit_ebins[-1]+1]
	dE = dE[list_fit_ebins[0]:list_fit_ebins[-1]+1]

	print('>>> Fitted Energy Range: %.2fTeV to %.2fTeV <<<' %((energy[0]-dE[0]/2)*MeV2TeV,(energy[-1]+dE[-1]/2)*MeV2TeV))

	# setup number of px and energy bins to reshape data
	nxpx = data.shape[2]
	nypx = data.shape[1]
	nebins = data.shape[0]

	# negative latitudes: b0 <= b <= b1
	# set right px index to slice data
	b_low = functions.lat2px(b0, 0., new_yref, new_pxs)
	b_high = functions.lat2px(b1, 0., new_yref, new_pxs)
	l_left = functions.lon2px(360.-l1, 0., new_xref, new_pxs)
	l_right = functions.lon2px(l1, 0., new_xref, new_pxs)

	if use_iTemp==True:
		# read references from input fb model fits file
		infb_pxs = fb_header['CDELT2']
		infb_yref = fb_header['CRPIX2']
		infb_xref = fb_header['CRPIX1']
		# set right px index to slice data
		b_input_low = functions.lat2px(b0, 0., infb_yref, infb_pxs)
		b_input_high = functions.lat2px(b1, 0., infb_yref, infb_pxs)
		l_input_left = functions.lon2px(360.-l1, 0., infb_xref, infb_pxs)
		l_input_right = functions.lon2px(l1, 0., infb_xref, infb_pxs)
		# bring input Temp in same form as data
		fb_temp = fb_temp[b_input_low:b_input_high,l_input_left:l_input_right]
		# plot
		fig_in_fb_temp = plt.figure()
		ax_in_fb_temp = fig_in_fb_temp.add_axes((.1,.1,.8,.8))
		ax_in_fb_temp.set_title('Input Bubbles Template')
		ax_in_fb_temp.set_xlabel('longitude [$^\circ$]')
		ax_in_fb_temp.set_ylabel('latitude [$^\circ$]')
		r_in = ax_in_fb_temp.imshow(fb_temp/np.amax(fb_temp), origin='lower', extent=(360-l1, -(360-l1), b0, b1))
		cbar_r_in = fig_in_fb_temp.colorbar(r_in, orientation='horizontal')
		# save plot
		if save_results==True:
			fig_in_fb_temp.savefig('use_iTemp/fit_fb_itemp.pdf')

	# reshape data and templates
	r_data = data.reshape(nebins, nxpx*nypx)
	r_fb_temp = fb_temp.reshape(nxpx*nypx)
	r_hard_temp = hard_temp.reshape(nxpx*nypx)
	r_soft_temp = soft_temp.reshape(nxpx*nypx)
	# number of eluminated fb px
	elum_px = np.count_nonzero(r_fb_temp)
	# number of px within the new px
	num_opx_in_npx = new_pxs/pxs

	#---------------#
	#	EXPOSURE	#
	#---------------#
	# load exposure
	exp_info = '%s_%s_%.2fE%.2f_%iEbins_%.2fpxs_t%i' %(irf[1], irf[0], Emin, Emax, nEbins, pxs, t_obs)
	exposure = functions.LinInterOfExp(PATH+'/response/expcube_%s.fits'%exp_info)
	# resize pxs of exposure to new_pxs
	exposure = functions.ResizePixel(exposure, new_pxs)

	# bring exposre into the same form as the sca data	
	# consider region used in sca and fitted erange
	exposure = exposure[list_fit_ebins[0]:list_fit_ebins[-1]+1,b_low:b_high,l_left:l_right]

	# use also only fb eluminated px in exposure
	for e in xrange(exposure.shape[0]):
		for i in xrange(fb_temp.shape[0]):
			for j in xrange(fb_temp.shape[1]):
				if fb_temp[i][j]==0:
					exposure[e][i][j]=0
				else:
					exposure[e][i][j] = exposure[e][i][j]/(num_opx_in_npx*num_opx_in_npx)
	# reshape exposure
	r_exposure = exposure.reshape(nebins, nxpx*nypx)

	#-------------------------------------------------------------------#
	#	CALC MEAN SIG^2 IN EACH ENERGY BIN ( sum_i(counts_e_i)/npx )	#
	#-------------------------------------------------------------------#
	# sigma2_i = data_i
	r_sigma2 = r_data
	# use also only fb eluminated px in sigma2
	for e in xrange(r_sigma2.shape[0]):
		for i in xrange(r_fb_temp.shape[0]):
			if r_fb_temp[i]==0:
				r_sigma2[e][i]=0

	#-----------------------------------------------------------#
	#	GENERATE ARRAY OF ALL TEMPLATE DATA (T[ntemp, npx]) 	#
	#-----------------------------------------------------------#
	if use_iTemp==False and use_sca_ibkg==False:
		# how many templates are used
		temp_list=[]
		for t in temp_dct.keys():
			if temp_dct[t]==True:
				temp_list.append(t)
		# number of used templates
		n_temps = len(temp_list)
		# initialize array of all templates used for modelling the data
		TEMP = np.zeros((n_temps, r_fb_temp.shape[0]))
		# fill the TEMP array with the data from the sca templates set to true in temp_dct
		print('>>> Take the Following Templates into Account <<<')
		for tidx, t in enumerate(temp_list):
			print('--- template: %s ---' %t)
			if t=='fb' and temp_dct[t]==True:
				TEMP[tidx] = r_fb_temp
			if t=='hard' and temp_dct[t]==True:
				TEMP[tidx] = r_hard_temp
			if t=='soft' and temp_dct[t]==True:
				TEMP[tidx] = r_soft_temp

	if use_iTemp==True and use_sca_ibkg==False:
		print('\n>>> USE INPUT FB TEMPLATE & INPUT BKG MODEL <<<\n')
		# load input bkg model
		info_bkgmod = '%s_%s_%.2fE%.2f_%iEbins_%.2fpxs_t%i' %(irf[1], irf[0], Emin, Emax, nEbins, pxs, t_obs)
		path2bkgmodel = PATH+'/modelcube/modelcube_gc_noFB_noBKG_%s.fits' %info_bkgmod
		bkg_temp, bkg_header = functions.LoadFitsData(path2bkgmodel,0)
		bkg_temp = functions.ResizePixel(bkg_temp, new_pxs)
		bkg_temp = np.sum(bkg_temp, axis=0)
		# bring input bkg model to the same form as the data
		# references
		bkg_xref = bkg_temp.shape[1]/2+.5
		bkg_yref = bkg_temp.shape[0]/2+.5
		# set right px index to slice data
		b_inbkg_low = functions.lat2px(b0, 0., bkg_yref, new_pxs)
		b_inbkg_high = functions.lat2px(b1, 0., bkg_yref, new_pxs)
		l_inbkg_left = functions.lon2px(360.-l1, 0., bkg_xref, new_pxs)
		l_inbkg_right = functions.lon2px(l1, 0., bkg_xref, new_pxs)
		# bring input Temp in same form as data
		bkg_temp = bkg_temp[b_inbkg_low:b_inbkg_high,l_inbkg_left:l_inbkg_right]
		# plot
		fig_in_bkg_temp = plt.figure()
		ax_in_bkg_temp = fig_in_bkg_temp.add_axes((.1,.1,.8,.8))
		ax_in_bkg_temp.set_title('Input BKG Template')
		ax_in_bkg_temp.set_xlabel('longitude [$^\circ$]')
		ax_in_bkg_temp.set_ylabel('latitude [$^\circ$]')
		r_in_bkg = ax_in_bkg_temp.imshow(bkg_temp/np.amax(bkg_temp), origin='lower', extent=(360-l1, -(360-l1), b0, b1))
		cbar_r_in_bkg = fig_in_bkg_temp.colorbar(r_in_bkg, orientation='horizontal')
		# save plot
		if save_results==True:
			fig_in_bkg_temp.savefig('use_iTemp/fit_bkg_itemp.pdf')
		# reshape bkg temp
		r_bkg_temp = bkg_temp.reshape(nxpx*nypx)
		# initialize the to templates
		temp_dct={'fb':True, 'bkg':True}
		# how many templates are used
		temp_list=[]
		for t in temp_dct.keys():
			if temp_dct[t]==True:
				temp_list.append(t)
		# number of used templates
		n_temps = len(temp_list)
		# initialize array of all templates used for modelling the data
		TEMP = np.zeros((n_temps, r_fb_temp.shape[0]))
		# fill the TEMP array with the data from the sca templates set to true in temp_dct
		print('>>> Take the Following Templates into Account <<<')
		for tidx, t in enumerate(temp_list):
			print('--- template: i%s ---' %t)
			if t=='fb' and temp_dct[t]==True:
				TEMP[tidx] = r_fb_temp
			if t=='bkg' and temp_dct[t]==True:
				TEMP[tidx] = r_bkg_temp
	if use_sca_ibkg==True and use_iTemp==False:
		print('\n>>> USE SCA FB TEMPLATE & INPUT BKG MODEL <<<\n')
		# load input bkg model
		info_bkgmod = '%s_%s_%.2fE%.2f_%iEbins_%.2fpxs_t%i' %(irf[1], irf[0], Emin, Emax, nEbins, pxs, t_obs)
		path2bkgmodel = PATH+'/modelcube/modelcube_gc_noFB_noBKG_%s.fits' %info_bkgmod
		bkg_temp, bkg_header = functions.LoadFitsData(path2bkgmodel,0)
		bkg_temp = functions.ResizePixel(bkg_temp, new_pxs)
		bkg_temp = np.sum(bkg_temp, axis=0)
		# bring input bkg model to the same form as the data
		# references
		bkg_xref = bkg_temp.shape[1]/2+.5
		bkg_yref = bkg_temp.shape[0]/2+.5
		# set right px index to slice data
		b_inbkg_low = functions.lat2px(b0, 0., bkg_yref, new_pxs)
		b_inbkg_high = functions.lat2px(b1, 0., bkg_yref, new_pxs)
		l_inbkg_left = functions.lon2px(360.-l1, 0., bkg_xref, new_pxs)
		l_inbkg_right = functions.lon2px(l1, 0., bkg_xref, new_pxs)
		# bring input Temp in same form as data
		bkg_temp = bkg_temp[b_inbkg_low:b_inbkg_high,l_inbkg_left:l_inbkg_right]
		# plot
		fig_in_bkg_temp = plt.figure()
		ax_in_bkg_temp = fig_in_bkg_temp.add_axes((.1,.1,.8,.8))
		ax_in_bkg_temp.set_title('Input BKG Template')
		ax_in_bkg_temp.set_xlabel('longitude [$^\circ$]')
		ax_in_bkg_temp.set_ylabel('latitude [$^\circ$]')
		r_in_bkg = ax_in_bkg_temp.imshow(bkg_temp/np.amax(bkg_temp), origin='lower', extent=(360-l1, -(360-l1), b0, b1))
		cbar_r_in_bkg = fig_in_bkg_temp.colorbar(r_in_bkg, orientation='horizontal')
		# save plot
		if save_results==True:
			fig_in_bkg_temp.savefig('use_iTemp/fit_bkg_itemp.pdf')
		# reshape bkg temp
		r_bkg_temp = bkg_temp.reshape(nxpx*nypx)
		# initialize the to templates
		temp_dct={'fb':True, 'bkg':True}
		# how many templates are used
		temp_list=[]
		for t in temp_dct.keys():
			if temp_dct[t]==True:
				temp_list.append(t)
		# number of used templates
		n_temps = len(temp_list)
		# initialize array of all templates used for modelling the data
		TEMP = np.zeros((n_temps, r_fb_temp.shape[0]))
		# fill the TEMP array with the data from the sca templates set to true in temp_dct
		print('>>> Take the Following Templates into Account <<<')
		for tidx, t in enumerate(temp_list):
			print('--- template: %s ---' %t)
			if t=='fb' and temp_dct[t]==True:
				TEMP[tidx] = r_fb_temp
			if t=='bkg' and temp_dct[t]==True:
				TEMP[tidx] = r_bkg_temp

	print('>>> Calc Matrices of the Problem <<<')
	#---------------------------------------------------#
	#	CALC MATRICES D(Data), M(Model) OF CHI^2 FIT  	#
	#---------------------------------------------------#
	D, M = CalcMatrices(r_data, TEMP, r_sigma2)

	#---------------#
	#	FITTING 	#
	#---------------#
	print('>>> Run Fit <<<')
	F = RunFit(D, M)

	#---------------#
	#	RESIDUALS 	#
	#---------------#
	r_R = r_data - np.dot(F, TEMP)
	R = r_R.reshape(nebins, nypx, nxpx)
	px_int_R = np.sum(np.sum(R, axis=2), axis=1)
	e_int_R = np.sum(R, axis=0)

	# setup color and shape
	colorlist, shape = functions.ColorAndMarker()

	# residual in each px integrated over energy
	fig_r = plt.figure()
	ax_r = fig_r.add_axes((0.125,0.1,0.8,0.8))
	ax_r.set_title('Residuals, E = (%.2f, %.2f)TeV' %(energy[0]*MeV2TeV,energy[-1]*MeV2TeV))
	ax_r.set_xlabel('longitude [$^\circ$]')
	ax_r.set_ylabel('latitude [$^\circ$]')
	r = ax_r.imshow(e_int_R, origin='lower', extent=(360-l1, -(360-l1), b0, b1))
	cbar_r = fig_r.colorbar(r, orientation='horizontal')

	# residuals in each energy bin integrated over px
	fig_rr = plt.figure()
	ax_rr = fig_rr.add_axes((0.125,0.1,0.8,0.8))
	ax_rr.set_title('Residuals')
	ax_rr.set_xlabel('E [TeV]')
	ax_rr.set_ylabel('residuals')
	ax_rr.grid(color=colorlist[-2], linestyle='--', linewidth='.5', alpha=0.25)
	ax_rr.axhline(y=0, xmin=0, xmax=1, color='k', linestyle=':', linewidth=2.)
	r_px_int = ax_rr.semilogx(energy*MeV2TeV, px_int_R, linestyle='', marker='o', markersize='5', color='r')

	#-------------------#
	#	SHOW RESIDUALS 	#
	#-------------------#
	if show_results==True:
		print('>>> SHOW RESIDUALS <<<')
		plt.show()
		plt.close('all')
	else:
		plt.close('all')


	#-------------------#
	#	CALC SPECTRUM	#
	#-------------------#
	print('>>> Calc Bubbles Flux in each Energy Bin and Pixel <<<')
	# create arrays
	r_fb_counts = np.zeros((nebins, nxpx*nypx))
	r_fb_flux = np.zeros((nebins, nxpx*nypx))
	fb_flux = np.zeros(nebins)
	av_fb_flux = np.zeros(nebins)
	for e in xrange(nebins):
		for p in xrange(nxpx*nypx):
			# calc modeld fb data in each px
			r_fb_counts[e] = F[e][0] * TEMP[0][p]
			# calc sca bubble flux in each px [ph/GeV/cm2/s]
			# if exposure[e]==0 or dE[e]==0:
			if r_exposure[e][p]==0 or dE[e]==0:
				continue
			else:
				# r_fb_flux[e][p] = r_fb_counts[e][p]/exposure[e]/(dE[e]*MeV2GeV)
				r_fb_flux[e][p] = r_fb_counts[e][p]/r_exposure[e][p]/(dE[e]*MeV2GeV)

	# calc sca bubbles flux in eluminated px
	print('>>> Calc. Total Bubbles Flux in each Energy Bin <<<')
	for e in xrange(nebins):
		fb_flux[e] = np.sum(r_fb_flux[e])
		# av_fb_flux[e] = np.sum(r_fb_flux[e])/elum_px

	#---------------#
	#	CALC ERROR 	#
	#---------------#
	# initialize arrays
	err_counts = np.zeros((nebins,n_temps))
	# err_flux = np.zeros((nebins,n_temps))
	err_flux = np.zeros((nxpx*nypx,nebins,n_temps))

	# calc error
	for e in xrange(nebins):
		if np.linalg.det(M[e])==0:
			continue
		else:
			# calc error in counts
			err_counts[e] = np.sqrt(np.diag(np.linalg.inv(M[e])))
	# calc error in flux
	for e in xrange(nebins):
		for p in xrange(nxpx*nypx):
			if r_exposure[e][p]!=0:
				err_flux[p][e] = err_counts[e] / r_exposure[e][p] / (dE[e]*MeV2GeV)
	err_flux = np.sum(err_flux, axis=0)

	#-------------------#
	#	PLOT SPECTRUM 	#
	#-------------------#
	# setup figure
	fig_spec = plt.figure()
	ax_spec = fig_spec.add_axes((.15,.1,0.8,0.8)) 	#left,bottom, width, height
	if use_iTemp==False:
		ax_spec.set_title('SCA Fermi Bubbles Spectrum')
	if use_iTemp==True:
		ax_spec.set_title('Input Fermi Bubbles Spectrum')
	ax_spec.set_ylabel(r'E$^2$ $\times$ dN/dE [GeV cm$^{-2}$ s$^{-1}$]')
	ax_spec.set_xlabel('E in [TeV]')
	ax_spec.grid(color=colorlist[-2], linestyle='--', linewidth='.5', alpha=0.25)
	ax_spec.loglog()
	ax_spec.set_xlim(1e-1, 2e2)
	# ax_spec.set_ylim(1e-10, 1e-8)
	# plot spectrum
	ax_spec.errorbar(energy*MeV2TeV, (energy*MeV2GeV)**2*fb_flux, yerr=err_flux.T[0],
			capsize=3, color='r', marker='o', markersize='5', linestyle='', label='flux')

	#-------------------------------#
	#	CALC EXPECTED BUBBLES FLUX 	#
	#-------------------------------#
	#=== Set up simulated ExpCutOff FB ===#
	E0=1e3    #[MeV]
	E_cut=5e7 #[MeV]
	k0=1e-10  #[1/MeV/cm2/s]
	index=-2
	FB = functions.ExpCutOffPowerLaw(energy, E0, E_cut, k0, index)/MeV2GeV #[1/GeV/cm2/s]

	# USE THE INPUT FB TEMPLATE TO CALCULATE THE EXPECTED FB FLUX IN ELUMINATED PX OF THE SCA FB TEMPLATE
	if use_iTemp==False:
		# name of the fermi bubbles input template
		path2InputTemp = 'FBmodel/input_map_fermi_bubbles.fits'
		if functions.FileExist1(path2InputTemp)==False:
			print('>>> no input_fb_temp found <<<')
			print path2InputTemp
			exit()
		# load input fb template
		input_fb_temp, input_fb_header = functions.LoadFitsData(path2InputTemp,0)
		# read references from input fb model fits file
		infb_pxs = input_fb_header['CDELT2']
		infb_yref = input_fb_header['CRPIX2']
		infb_xref = input_fb_header['CRPIX1']
		# set right px index to slice data
		b_input_low = functions.lat2px(b0, 0., infb_yref, infb_pxs)
		b_input_high = functions.lat2px(b1, 0., infb_yref, infb_pxs)
		l_input_left = functions.lon2px(360.-l1, 0., infb_xref, infb_pxs)
		l_input_right = functions.lon2px(l1, 0., infb_xref, infb_pxs)
		# bring input Temp in same form as data
		fb_itemp = input_fb_temp[b_input_low:b_input_high,l_input_left:l_input_right]
		# consider only eluminated px of the sca fb template
		for i in xrange(fb_temp.shape[0]):
			for j in xrange(fb_temp.shape[1]):
				if fb_temp[i][j]==0:
					fb_itemp[i][j]=0
	else:
		fb_itemp = fb_temp

	#Calc tot. expected flux in eluminated px of the sca fb template
	itemp_dO = np.sum(fb_itemp*dO)
	FB_flux = FB*itemp_dO

	# flux in all px
	# FB_flux = FB*elum_px*dO # sum over elum_px f temp dO

	# Plt the spectrum
	ax_spec.plot(energy*MeV2TeV, (energy*MeV2GeV)**2*FB_flux, color=colorlist[-1], linestyle='--', label='expected flux')
	ax_spec.legend(loc='best', ncol=2, frameon=True, framealpha=0.25)

	#----------------------#
	#	SHOW/SAVE RESULTS  #
	#----------------------#
	if save_results==True:
		print('>>> SAVE RESULTS <<<')
		if finalplots==True:
			if use_iTemp==True:
				fig_spec.savefig('use_iTemp/spec.pdf')
				fig_r.savefig('use_iTemp/fit_res.pdf')
				fig_rr.savefig('use_iTemp/fit_res_e_.pdf')
			else:
				fig_spec.savefig(path2results+'/spec_%s.pdf' %info_temp)
				fig_r.savefig(path2results+'/fit_res_%s.pdf' %info_temp)
				fig_rr.savefig(path2results+'/fit_res_e_%s.pdf' %info_temp)
		else:
			if use_iTemp==True:
				fig_spec.savefig('use_iTemp/spec.png')
				fig_r.savefig('use_iTemp/fit_res.png')
				fig_rr.savefig('use_iTemp/fit_res_e.png')
			else:
				fig_spec.savefig(path2results+'/spec_%s.png' %info_temp)
				fig_r.savefig(path2results+'/fit_res_%s.png' %info_temp)
				fig_rr.savefig(path2results+'/fit_res_e_%s.png' %info_temp)

	if show_results==True:
		print('>>> SHOW RESULTS <<<')
		plt.show()
	else:
		plt.close('all')



if __name__ == '__main__':
	Fit2Data()












