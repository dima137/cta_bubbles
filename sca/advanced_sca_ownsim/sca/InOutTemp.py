import os
import shutil
import pyfits as pf
import numpy as np
import matplotlib.pyplot as plt
#--- own modules ---#
import functions
import OnOffMethod

def InputTemp(path2InTemp,
	l0=360., l1=355., b0=-5., b1=-1., lbin_size=.5, bbin_size=.5,
	show_results=False):
	#INPUT
	#OUTPUT

	# check if the files exits
	if functions.FileExist1(path2InTemp)==False:
		print('>>> NO INPUT TEMPLATE FOUND <<<')
		exit()

	# open input template
	iTempHDU = pf.open(path2InTemp)
	iTemp = iTempHDU[0].data
	iTemp_header = iTempHDU[0].header

	# read out info from iTemp header
	ipxs = iTemp_header['CDELT2']
	iref_xpx = iTemp_header['CRPIX1']
	iref_ypx = iTemp_header['CRPIX2']
	iref_ldeg = iTemp_header['CRVAL1']
	iref_bdeg = iTemp_header['CRVAL2']

	# how many px have the Fermi-LAT bubbles -> estimate omega of the FB
	npx=0
	for x in xrange(iTemp.shape[0]):
		for y in xrange(iTemp.shape[1]):
			if iTemp[x][y]!=0:
				npx+=1

	# how large are the FB on the sky, according to the input template := Omega (~1sr)
	dO = functions.dOmega(ipxs)
	Omega_fb = npx*dO

	# input template in the ROI
	iTemp = OnOffMethod.ON(iTemp,
						l0=l0, l1=l1, b0=b0, b1=b1, lbin_size=lbin_size, bbin_size=bbin_size, 
						ref_ldeg=iref_ldeg, ref_bdeg=iref_bdeg, ref_xpx=iref_xpx, ref_ypx=iref_ypx, pxs=ipxs)

	# normalize it to 1
	iTemp = iTemp/np.amax(iTemp)

	# set up correct reference px and deg
	ref_l = l0
	ref_b = b1
	ref_x = 0.5
	ref_y = iTemp.shape[0]+.5
	new_pxs = lbin_size
	# print FB template to fits file
	ROIinfo = '%.2fl%.2f_%.2fb%.2f_%.2fpxs' %(l0,l1,b0,b1,new_pxs)
	name4iTemp = 'input_temp_%s.fits' %ROIinfo
	if functions.FileExist1('sca_temp/'+name4iTemp)==False:
		print('>>> PRINT NEW FB TEMPLATE (ROI) <<<')
		functions.Print2Fits(iTemp, new_pxs, ref_x, ref_y, ref_l, ref_b, 'sca_temp/'+name4iTemp)
	else:
		print('>>> FB TEMPLATE (ROI) EXISTS ALREADY - FLUSH TEMPLATE <<<')
		temp = pf.open('sca_temp/'+name4iTemp, mode='update')
		temp[0].data = iTemp
		temp.flush()
		temp.close()

	return iTemp

def OutputTemp(path2outTemp,
	l0=360., l1=355., b0=-5., b1=-1., lbin_size=.5, bbin_size=.5,
	show_results=True):
	#INPUT
	#OUTPUT

	# check if the files exits
	if functions.FileExist1(path2outTemp)==False:
		print('>>> NO OUTPUT TEMPLATE FOUND <<<')
		exit()

	# open input template
	oTempHDU = pf.open(path2outTemp)
	oTemp = oTempHDU[0].data
	oTemp_header = oTempHDU[0].header

	# read out info from iTemp header
	opxs = oTemp_header['CDELT2']
	oref_xpx = oTemp_header['CRPIX1']
	oref_ypx = oTemp_header['CRPIX2']
	oref_ldeg = oTemp_header['CRVAL1']
	oref_bdeg = oTemp_header['CRVAL2']

	# how many px have the Fermi-LAT bubbles -> estimate omega of the FB
	npx=0
	for x in xrange(oTemp.shape[0]):
		for y in xrange(oTemp.shape[1]):
			if oTemp[x][y]!=0:
				npx+=1

	# how large are the FB on the sky, according to the input template := Omega (~1sr)
	dO = functions.dOmega(opxs)
	Omega_fb = npx*dO

	# # input template in the ROI
	# oTemp = OnOffMethod.ON(oTemp,
	# 					l0=l0, l1=l1, b0=b0, b1=b1, lbin_size=lbin_size, bbin_size=bbin_size, 
	# 					ref_ldeg=oref_ldeg, ref_bdeg=oref_bdeg, ref_xpx=oref_xpx, ref_ypx=oref_ypx, pxs=opxs)
	oTemp = oTemp[:,oTemp.shape[1]/2:oTemp.shape[1]]

	# normalize it to 1
	oTemp = oTemp/np.amax(oTemp)

	# set up correct reference px and deg
	ref_l = l0
	ref_b = b1
	ref_x = 0.5
	ref_y = oTemp.shape[0]+.5
	new_pxs = lbin_size
	# print FB template to fits file
	ROIinfo = '%.2fl%.2f_%.2fb%.2f_%.2fpxs' %(l0,l1,b0,b1,new_pxs)
	name4oTemp = 'output_temp_%s.fits' %ROIinfo
	if functions.FileExist1('sca_temp/'+name4oTemp)==False:
		print('>>> PRINT NEW SCA FB TEMPLATE (ROI) <<<')
		functions.Print2Fits(oTemp, new_pxs, ref_x, ref_y, ref_l, ref_b, 'sca_temp/'+name4oTemp)
	else:
		print('>>> FB TEMPLATE (ROI) EXISTS ALREADY - FLUSH TEMPLATE <<<')
		temp = pf.open('sca_temp/'+name4oTemp, mode='update')
		temp[0].data = oTemp
		temp.flush()
		temp.close()

	return oTemp









if __name__ == '__main__':

	PATH = os.getenv('SCA')
	oTemp_path = PATH+'/sca_temp/sca_fb_temp_2sig_gc_ExpCut_FermiBubbles_noBKG_exr_2deg_stripe_Hard_-1.90_Soft_-2.70_1dc_South_z20_50h_pxs0.50_0.11E12.19_t180000.fits'
	iTemp_path = PATH+'/FBmodel/input_map_fermi_bubbles.fits'
	iTemp = InputTemp(iTemp_path)
	oTemp = OutputTemp(oTemp_path)

	for i in xrange(iTemp.shape[0]):
		for j in xrange(iTemp.shape[1]):
			if oTemp[i][j]!=0:
				print iTemp[i][j], oTemp[i][j]

	res_temp = (oTemp-iTemp)/iTemp
	plt.imshow(res_temp, origin='lower')
	cbar = plt.colorbar()
	plt.show()
