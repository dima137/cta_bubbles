import os
import shutil
import ctools
import cscripts
import numpy as np
import pyfits as pf
import matplotlib.pyplot as plt
import xml.etree.ElementTree as et
#--- own modules ---#
import dio
import functions
import OnOffMethod

def CompSrc(name4soft, name4hard):
	#-------------------------------------------------------#
	#														#
	#		LOOK FOR POINT SOURCES IN BKG TEMPLATES 		#
	#														#
	#-------------------------------------------------------#
	print('>>> LOOK FOR POINT SOURCES IN BKG TEMPLATES <<<')
	# initialize parameter
	detected = []
	line=[]
	ra = 0
	dec = 0
	# parameter
	gaussian_sigma = 2
	bkgmap_dict = {'softbkg':name4soft, 'hardbkg':name4hard}

	for temp_map in bkgmap_dict.keys():
		inputmap = 'sca_temp/bkg_model/'+bkgmap_dict[temp_map]
		if 'softbkg' in bkgmap_dict[temp_map]:
			outmodel = 'sca_temp/bkg_model/sca_softbkg_pntsrc.xml'
			outds9file = 'sca_temp/bkg_model/sca_softbkg_pntsrc_loc.reg'
		if 'hardbkg' in bkgmap_dict[temp_map]:
			outmodel = 'sca_temp/bkg_model/sca_hardbkg_pntsrc.xml'
			outds9file = 'sca_temp/bkg_model/sca_hardbkg_pntsrc_loc.reg'

		# run cssrcdetect
		srcdetect = cscripts.cssrcdetect()
		srcdetect['inmap'] = inputmap
		srcdetect['outmodel'] = outmodel
		srcdetect['outds9file'] = outds9file
		srcdetect['exclrad'] = 1.
		srcdetect['srcmodel'] = 'POINT'
		srcdetect['bkgmodel'] = 'NONE'
		srcdetect['threshold'] = gaussian_sigma
		srcdetect.execute()

		# read out position from xml file
		# load xml file
		tree = et.parse(outmodel)
		root = tree.getroot()
		for src in root.iter('source'):
			for loc in src.iter('spatialModel'):
				for par in loc.iter('parameter'):
					if par.get('name')=='RA':
						ra = par.get('value')
					if par.get('name')=='DEC':
						dec = par.get('value')
			name_ra_dec = [temp_map, ra, dec]
			detected.append(name_ra_dec)

		#read out xml files line-by-line
		with open(outmodel, "r") as f:
			if 'softbkg' in bkgmap_dict[temp_map]: 
				line_soft = f.readlines()
				for l in xrange(len(line_soft)):
					line.append(line_soft[l])
			if 'hardbkg' in bkgmap_dict[temp_map]:
				line_hard = f.readlines()
				for l in xrange(len(line_hard)):
					line.append(line_hard[l])
	# merge the two output xml files
	if functions.FileExist1('sca_temp/bkg_model/bkg_src.xml')==True:
		os.remove('sca_temp/bkg_model/bkg_src.xml')
	file = open("sca_temp/bkg_model/bkg_src.xml", "w")
	for l in xrange(len(line)):
		file.write(line[l])
	file.close()

	# # -----------------------------------------------------------#
	# # 															#
	# # 		EXCLUDE THE FOUND POINT SOURCES FROM TEMPLATES 		#
	# # 															#
	# # -----------------------------------------------------------#
	# for sidx, src_loc in enumerate(detected):
	# 	# transform position to galactic coordinates
	# 	ra, dec = src_loc[1], src_loc[2]
	# 	lon, lat = functions.icrs2gal(ra, dec)
	# 	# transform coordinates to px index
	# 	if 'softbkg' in src_loc:
	# 		sdata, sheader = functions.LoadFitsData('sca_temp/bkg_model/'+name4soft, 0) 
	# 		xref = sheader['CRPIX1']
	# 		yref = sheader['CRPIX2']
	# 		lref = sheader['CRVAL1']
	# 		bref = sheader['CRVAL2']
	# 		new_pxs = sheader['CDELT2']
	# 	if 'hardbkg' in src_loc:
	# 		hdata, hheader = functions.LoadFitsData('sca_temp/bkg_model/'+name4hard, 0) 
	# 		xref = hheader['CRPIX1']
	# 		yref = hheader['CRPIX2']
	# 		lref = hheader['CRVAL1']
	# 		bref = hheader['CRVAL2']
	# 		new_pxs = hheader['CDELT2']
	# 	xpx, ypx = functions.lon2px(lon, lref, xref, new_pxs), functions.lat2px(lat, bref, yref, new_pxs)
	# 	# set px to zero for the corresponding template
	# 	if 'softbkg' in src_loc:
	# 		tmap = pf.open('sca_temp/bkg_model/'+name4soft, mode='update')
	# 	if 'hardbkg' in src_loc:
	# 		tmap = pf.open('sca_temp/bkg_model/'+name4hard, mode='update')
	# 	counts = tmap[0].data
	# 	for y in xrange(counts.shape[0]):
	# 		if y==ypx:
	# 			for x in xrange(counts.shape[1]):
	# 				if x==xpx:
	# 					counts[y][x]=0
	# 		else:
	# 			continue
	# 	# write new data to fits file
	# 	tmap[0].data=counts
	# 	tmap.flush()
	# 	tmap.close()

if __name__ == '__main__':
	
	soft = 'sca_softbkg_temp_3sig_gc_ExpCut_FermiBubbles_noBKG_exr_2deg_stripe_Hard_-2.00_Soft_-2.50_1dc_South_z20_50h_pxs1.00_0.03E160.00_t180000.fits'
	hard = 'sca_hardbkg_temp_3sig_gc_ExpCut_FermiBubbles_noBKG_exr_2deg_stripe_Hard_-2.00_Soft_-2.50_1dc_South_z20_50h_pxs1.00_0.03E160.00_t180000.fits'
	CompSrc(soft, hard)
